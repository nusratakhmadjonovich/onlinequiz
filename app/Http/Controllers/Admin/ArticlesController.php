<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Libraries\UploadManager;
use App\Model\Admin\Articles;
use App\Model\Admin\Fanlar;
use App\Model\Admin\Javoben;
use App\Model\Admin\Javoboz;
use App\Model\Admin\Javobru;
use App\Model\Admin\Javobuz;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use PhpParser\Node\Stmt\Return_;
use Yajra\DataTables\DataTables;

class ArticlesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function list(Request $request)
    {
        $users = Articles::where('status', '!=', -1)->get();
        return Datatables::of($users)
            ->addColumn('status', function ($user) {
                if($user->status == 1)
                    return '<span class="label label-success">Актив</span>';
                else
                    return '<span class="label label-danger">Неактив</span>';
            })
            ->addColumn('actions', function ($user) {
                return '
                    <a href="'. url('/admin/articles/' . $user->id) .'" title="Смотрить"><button class="btn btn-success btn-sm"><i class="fa fa-eye" aria-hidden="true"></i> </button></a>
                    <a href="'. url('/admin/articles/' . $user->id . '/edit') .'" title="Редактировать"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> </button></a>
                    
                    <form method="POST" action="'. url('/admin/articles' . '/' . $user->id).'" accept-charset="UTF-8" style="display:inline">
                        '.method_field("DELETE").'
                        '.csrf_field() .'
                        <button type="submit" class="btn btn-danger btn-sm" title="Удалить" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o" aria-hidden="true"></i> </button>
                    </form>
                ';
            })
            ->rawColumns(['actions', 'status'])
            ->filter(function ($query) {
                if (request()->filled('title_uz-Latn')) {
                    $query->where('title_uz-Latn', 'like', "%".request('title_uz-Latn')."%");
                }

            })
            ->make(true);
    }

    public function index()
    {
        return view('admin.articles.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $article = New Articles();
        $article->status = -1;
        $article->save();
        return redirect('admin/articles/'.$article->id.'/edit');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $find_articles = Articles::findorfail($id);
        return view('admin.articles.show',compact('find_articles'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $post = Articles::findorfail($id);

        $fanlar = Fanlar::all();
        $isNewRecord = (int)$post->status == -1;
        $javob_oz = Javoboz::where('savol_id','=',$id)->get();
        $javob_uz = Javobuz::where('savol_id','=',$id)->get();
        $javob_ru = Javobru::where('savol_id','=',$id)->get();
        $javob_en = Javoben::where('savol_id','=',$id)->get();

        return view('admin.articles.edit',compact(['post','isNewRecord','fanlar','javob_oz','javob_uz','javob_ru','javob_en']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $post = Articles::findorfail($id);
        $requestData = $request->all();

        if ($request->file('photo')) {
            $requestData['photo'] = UploadManager::uploadPhoto('article', $post->id, $request->file('photo'), $post->photo);
            $post['photo'] = $requestData['photo'];
        }
        if ($request->file('file'))
        {
            $requestData['file'] = UploadManager::upload('files', $post->id, $request->file('file'), $post->file);
            $post['file'] = $requestData['file'];
        }


        $post->status = 0;
        $post->fanlar_id = $request->article_category_id;
        $post->update($requestData);

        foreach ($request->javob['uz'] as $key_uz=>$item_uz)
        {
            if ($item_uz != '')
            {
                $javob_uz = new Javobuz();
                $javob_uz->savol_id = $id;
                $javob_uz->javoblar = $item_uz;
                $javob_uz->save();
            }
        }

        foreach($request->javob['oz'] as $key_oz=>$item_oz){
            if ($item_oz != '')
            {
                $javob_oz = New Javoboz();
                $javob_oz->savol_id = $id;
                $javob_oz->javoblar = $item_oz;
                $javob_oz->save();
            }
        }

        foreach($request->javob['ru'] as $key_ru=>$item_ru){
            if ($item_ru != '')
            {
                $javob_ru = New Javobru();
                $javob_ru->savol_id = $id;
                $javob_ru->javoblar = $item_ru;
                $javob_ru->save();
            }
        }

        foreach($request->javob['en'] as $key_en=>$item_en){
            if ($item_en != '')
            {
                $javob_en = New Javoben();
                $javob_en->savol_id = $id;
                $javob_en->javoblar = $item_en;
                $javob_en->save();
            }
        }


        return redirect('/admin/articles/'.$post->id.'/edit')->with('success-message','Ma`lumot o`zgartirildi');
    }
    public function removePhoto($id)
    {
        $article = Articles::findOrFail($id);
        if($article->photo) {
            UploadManager::delete('article', $article->id, $article->photo);
            $article->photo = '';
            $article->update();
        }

        return back()->with('success-message', 'Фотография удалено успешно');
    }

    public function removeFile($id)
    {
        $article = Articles::findOrFail($id);
        if ($article->file) {
            UploadManager::delete('files', $article->id, $article->file);
            $article->file = '';
            $article->update();
        }

        return back()->with('success-message', 'файл удалено успешно');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delete = Articles::findorfail($id);
        $delete->delete();
        return redirect('admin/articles');
    }

    public function update_answer_to_question(Request $request,$test)
    {
        if ($request->lang == 'oz')
        {
            $answer = Javoboz::findorfail($request->category_id);
            $answer->javoblar = $request->javoblar;
            if (Input::get('correct_answer',true))
            {
                $answer->togri_javob = 1;
            }else
            {
                $answer->togri_javob = 0;
            }
            $answer->update();

            return redirect('admin/articles/'.$request->ques_id.'/edit#tab_4');
        }

        if ($request->lang == 'uz')
        {
            $answer = Javobuz::findorfail($request->category_id);
            $answer->javoblar = $request->javoblar;
            if (Input::get('correct_answer',true))
            {
                $answer->togri_javob = 1;
            }else
            {
                $answer->togri_javob = 0;
            }
            $answer->update();

            return redirect('admin/articles/'.$request->ques_id.'/edit#tab_3');
        }

        if ($request->lang == 'ru')
        {
            $answer = Javobru::findorfail($request->category_id);
            $answer->javoblar = $request->javoblar;
            if (Input::get('correct_answer',true))
            {
                $answer->togri_javob = 1;
            }else
            {
                $answer->togri_javob = 0;
            }
            $answer->update();

            return redirect('admin/articles/'.$request->ques_id.'/edit#tab_1');
        }

        if ($request->lang == 'en')
        {
            $answer = Javoben::findorfail($request->category_id);
            $answer->javoblar = $request->javoblar;
            if (Input::get('correct_answer',true))
            {
                $answer->togri_javob = 1;
            }else
            {
                $answer->togri_javob = 0;
            }
            $answer->update();

            return redirect('admin/articles/'.$request->ques_id.'/edit#tab_2');
        }
    }

    public function delete_answer_from_question(Request $request)
    {
        if ($request->lang == 'uz')
        {
            $answer = Javoboz::findorfail($request->trashid);
            $answer->delete();
            return redirect('admin/articles/'.$request->ques_id.'/edit#tab_4');
        }

        if ($request->lang == 'uz')
        {
            $answer = Javobuz::findorfail($request->trashid);
            $answer->delete();
            return redirect('admin/articles/'.$request->ques_id.'/edit#tab_3');
        }

        if ($request->lang == 'ru')
        {
            $answer = Javobru::findorfail($request->trashid);
            $answer->delete();
            return redirect('admin/articles/'.$request->ques_id.'/edit#tab_1');
        }

        if ($request->lang == 'en')
        {
            $answer = Javoben::findorfail($request->trashid);
            $answer->delete();
            return redirect('admin/articles/'.$request->ques_id.'/edit#tab_2');
        }
    }
}
