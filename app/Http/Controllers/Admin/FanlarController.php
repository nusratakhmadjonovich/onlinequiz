<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Model\Admin\Fanlar;
use Illuminate\Http\Request;

class FanlarController extends Controller
{
    public  function fanlar()
    {
        $name = Fanlar::all();
        return view('admin.fanlar.index',['name'=>$name]);
    }
    public function creat()
    {
        return view('admin.fanlar.creat');
    }
    public function post_creat(Request $request)
    {
       $fanlar = New Fanlar();
       $fanlar->fanlar_name = $request->name;
       $fanlar->time = $request->time;
       $fanlar->test_count = $request->test_count;
       $fanlar->kiritilgan_vaqti = $request->vaqti;
       $fanlar->save();

       return redirect('/admin/fanlar');
    }
    public function edit($id)
    {
        $fanlar = Fanlar::where('id','=',$id)->first();
        return view('admin.fanlar.edit',compact('fanlar'));
    }
    public function update(Request $request,$id)
    {
        $fanlar = Fanlar::findorfail($id);
        $fanlar->fanlar_name = $request->name;
        $fanlar->time = $request->time;
        $fanlar->test_count = $request->test_count;
        $fanlar->kiritilgan_vaqti = $request->vaqti;
        $fanlar->update();
        return redirect('/admin/fanlar');
    }
    public function delete($id)
    {
        $fanlar = Fanlar::findorfail($id);
        $fanlar->delete();
        return redirect('/admin/fanlar');
    }

}
