<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Model\Admin\Customer;
use Illuminate\Foundation\Auth\User;
use Illuminate\Http\Request;
use PDF;
use Excel;
use App\Exports\DataExport;
use Exception;

class AdminController extends Controller
{
    public function admin_index()
    {
        return view('admin.index');
    }
    public function results()
    {
        return view('admin.results.index');
    }
    public function results2()
    {
        return view('admin.results.otmaganlar');
    }
    public function admin_register()
    {
        $register = User::where('is_deleted','=','0')->get();
        return view('admin.register.index',['register'=>$register]);
    }
    public function admin_register_creat()
    {
        return view('admin.register.create');
    }

    public function edit_register_user($id)
    {
        $user = User::findorfail($id);
        //$requestData = $request->all();
       // $update_user->update($update_user);

        return view('admin.register.edit',compact('user'));

    }

    public function update_register_post_user(Request $request,$id)
    {
        $update_user = Customer::findorfail($id);

        $requestData = $request->all();

        if($request->password && $request->password == $request->confirm_password) {
            $requestData['password'] = bcrypt($request->password);
        }else {
            unset($requestData['password']);
        }

        $update_user->update($requestData);

        return redirect('admin/register/edit/'.$id)->with('success-message','Ma`lumot o`zgartirildi');

    }

    public function post_register(Request $request){
        $user = new Customer();
        $user->name = $request->name;
        $user->lastname = $request->lastname;
        $user->email = $request->email;
        $user->role = $request->role;
        $user->status = $request->status;
        $user->password = bcrypt($request->password);
        $user->remember_token = $request['_token'];
        $user->save();
        return redirect('admin/register')->with('success-message','Ma`lumot o`zgartirildi');
    }

    public function admin_register_delete($id)
    {
        $register = User::findorfail($id);
        $register->is_deleted = 1;
        $register->update();
        return redirect('/admin/register')->with('success-message','Ma`lumot o`zgartirildi');
    }

    public function generatePDF()
    {
        $pdf = User::all();
        $pdf = PDF::loadView('myPDF',compact('pdf'));

        return $pdf->download('index.pdf');

        /*return view('myPDF',compact('pdf'));*/
    }

    public function create()
    {
        return view('product');
    }

    public function exportFile()
    {
        return Excel::download(new DataExport, 'index.xlsx');
    }
    public function generateDocx()
    {
        $phpWord = new \PhpOffice\PhpWord\PhpWord();

        $section = $phpWord->addSection();

        $description = "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
            quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
            consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
            cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
            proident, sunt in culpa qui officia deserunt mollit anim id est laborum.";

        $section->addImage("http://itsolutionstuff.com/frontTheme/images/logo.png");
        $section->addText($description);

        $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
        try {
            $objWriter->save(storage_path('helloWorld.docx'));
        } catch (Exception $e) {
        }

        return response()->download(storage_path('helloWorld.docx'));
    }

}
