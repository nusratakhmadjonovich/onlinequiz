<?php

namespace App\Http\Controllers\Reviewer;

use App\Http\Controllers\Controller;
use App\Model\Admin\Articles;
use Illuminate\Http\Request;

class TestResultsController extends Controller
{
    public function index($user_id,$random_id)
    {
        if (app()->getLocale() == 'uz-Latn')
        {
            $question_from_user = Articles::join('oz_users_answers', 'oz_users_answers.qive_answer_id', '=', 'articles.id')
                ->where('oz_users_answers.user_id','=',\Illuminate\Support\Facades\Auth::user()->id)
                ->where('oz_users_answers.count_id','=',$random_id)
                ->select('articles.id','articles.description_uz-Latn','articles.description_uz','articles.description_ru','articles.description_en','oz_users_answers.qive_answer_id','oz_users_answers.answer_id')
                ->orderBy('id','ASC')
                ->get();
        }

        if (app()->getLocale() == 'uz')
        {
            $question_from_user = Articles::join('uz_users_answers', 'uz_users_answers.qive_answer_id', '=', 'articles.id')
                ->where('uz_users_answers.user_id','=',\Illuminate\Support\Facades\Auth::user()->id)
                ->where('uz_users_answers.count_id','=',$random_id)
                ->select('articles.id','articles.description_uz-Latn','articles.description_uz','articles.description_ru','articles.description_en','uz_users_answers.qive_answer_id','uz_users_answers.answer_id')
                ->orderBy('id','ASC')
                ->get();
        }

        if (app()->getLocale() == 'ru')
        {
            $question_from_user = Articles::join('ru_users_answers', 'ru_users_answers.qive_answer_id', '=', 'articles.id')
                ->where('ru_users_answers.user_id','=',\Illuminate\Support\Facades\Auth::user()->id)
                ->where('ru_users_answers.count_id','=',$random_id)
                ->select('articles.id','articles.description_uz-Latn','articles.description_uz','articles.description_ru','articles.description_en','ru_users_answers.qive_answer_id','ru_users_answers.answer_id')
                ->orderBy('id','ASC')
                ->get();
        }

        if (app()->getLocale() == 'en')
        {
            $question_from_user = Articles::join('en_users_answers', 'en_users_answers.qive_answer_id', '=', 'articles.id')
                ->where('en_users_answers.user_id','=',\Illuminate\Support\Facades\Auth::user()->id)
                ->where('en_users_answers.count_id','=',$random_id)
                ->select('articles.id','articles.description_uz-Latn','articles.description_uz','articles.description_ru','articles.description_en','en_users_answers.qive_answer_id','en_users_answers.answer_id')
                ->orderBy('id','ASC')
                ->get();
        }

        return view('reviewer.results',compact(['question_from_user','random_id','user_id']));
    }
}
