<?php

namespace App\Http\Controllers\Reviewer;

use App\ENuseranswer;
use App\Http\Controllers\Controller;
use App\Model\Admin\Articles;
use App\Model\Admin\Fanlar;
use App\Model\Admin\Javoben;
use App\Model\Admin\Javoboz;
use App\Model\Admin\Javobru;
use App\Model\Admin\Javobuz;
use App\Model\Reviewer\Userresult;
use App\Ozuseranswer;
use App\RUuseranswer;
use App\Uzuseranswer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;

class ReviewerController extends Controller
{
    public function dashboard()
    {
        $fanlar = Fanlar::all();
        return view('reviewer.dashboard',compact('fanlar'));
    }

    public function index($id){
        $science  = Fanlar::findorfail($id);
        return view('reviewer.index',['science'=>$science]);
    }

    public function blog($id,$random){

        $user_id = Auth::user()->id;

        if (app()->getLocale() == 'uz-Latn')
        {
            $javoblar = Javoboz::where('savol_id','=',$id)->get();
            $next = Ozuseranswer::where('qive_answer_id','>',$id)
                ->where('user_id','=',$user_id)
                ->where('count_id','=',$random)
                ->min('qive_answer_id');
        }elseif (app()->getLocale() == 'uz')
        {
            $javoblar = Javobuz::where('savol_id','=',$id)->get();
            $next = Uzuseranswer::where('qive_answer_id','>',$id)
                ->where('user_id','=',$user_id)
                ->where('count_id','=',$random)
                ->min('qive_answer_id');
        }elseif (app()->getLocale() == 'ru')
        {
            $javoblar = Javobru::where('savol_id','=',$id)->get();
            $next = RUuseranswer::where('qive_answer_id','>',$id)
                ->where('user_id','=',$user_id)
                ->where('count_id','=',$random)
                ->min('qive_answer_id');
        }else
        {
            $javoblar = Javoben::where('savol_id','=',$id)->get();
            $next = ENuseranswer::where('qive_answer_id','>',$id)
                ->where('user_id','=',$user_id)
                ->where('count_id','=',$random)
                ->min('qive_answer_id');
        }

        $savol = Articles::where('id','=',$id)->first();

        $question_from_user = Articles::join('oz_users_answers', 'oz_users_answers.qive_answer_id', '=', 'articles.id')
            ->where('oz_users_answers.user_id','=',\Illuminate\Support\Facades\Auth::user()->id)
            ->where('oz_users_answers.count_id','=',$random)
            ->select('articles.id','articles.description_uz-Latn','oz_users_answers.qive_answer_id','oz_users_answers.answer_id')
            ->orderBy('id','ASC')
            ->get();


        return view('reviewer.blog',compact(['javoblar','savol','random','question_from_user','id']))->with('next',$next);
    }

    public function dublicate_blog($savol_id){
        $find_fanlar = Articles::where('id','=',$savol_id)->first();
        $fanlar = Fanlar::where('id','=',$find_fanlar->fanlar_id)->first();
        $savol = Articles::where('status','=',1)->inRandomOrder()->take(3)->get();
        $array = array();
        foreach ($savol as $key=>$item)
        {
            $array[] = $item->id;
        }
        $javoblar = Javoboz::where('savol_id','=',$savol_id)->get();
        $prev = Articles::where('id', '<', $savol_id)->max('id');
        //$next = Articles::where('id', '>', $savol_id)->min('id');
        $transport = array('foot', 'bike', 'car', 'plane');
        $next = next($array);

        return view('reviewer.blog',compact(['javoblar','savol','fanlar','savol_id']))->with('prev',$prev)->with('next',$next);
    }


    public function user_answer(Request $request)
    {
        if (app()->getLocale() == 'uz-Latn')
        {
            $resul2 =  Ozuseranswer::where('user_id','=',$request->auth_id)
                ->where('qive_answer_id','=',$request->savol_id)
                ->where('count_id','=',$request->random_id)
                ->first();
        }

        if (app()->getLocale() == 'uz')
        {
            $resul2 =  Uzuseranswer::where('user_id','=',$request->auth_id)
                ->where('qive_answer_id','=',$request->savol_id)
                ->where('count_id','=',$request->random_id)
                ->first();
        }

        if (app()->getLocale() == 'ru')
        {
            $resul2 =  RUuseranswer::where('user_id','=',$request->auth_id)
                ->where('qive_answer_id','=',$request->savol_id)
                ->where('count_id','=',$request->random_id)
                ->first();
        }

        if (app()->getLocale() == 'en')
        {
            $resul2 =  ENuseranswer::where('user_id','=',$request->auth_id)
                ->where('qive_answer_id','=',$request->savol_id)
                ->where('count_id','=',$request->random_id)
                ->first();
        }

        $resul2->answer_id = $request->message_pri;
        $resul2->update();

        return $resul2;

    }

    public function user_result(Request $request)
    {


        if (app()->getLocale() == 'uz-Latn')
        {
            $resul2 =  Ozuseranswer::where('user_id','=',$request->auth_id)
                ->where('qive_answer_id','=',$request->savol_id)
                ->where('count_id','=',$request->random_id)->first();


            if ($resul2->answer_id != 0)
            {
                return 'Siz bu savolni oldin tanlab bo\'lgansiz ';
            }
            $resul2->answer_id = $request->message_pri;
            $resul2->update();

            $leagues = \DB::table('javobozs')
                ->select('*')
                ->join('oz_users_answers', 'javobozs.id', '=', 'oz_users_answers.answer_id')
                ->where('javobozs.togri_javob', 1)
                ->where('oz_users_answers.user_id', Auth::user()->id)
                ->where('oz_users_answers.count_id', $request->random_id)
                ->count();


            $user_result = new Userresult();
            $user_result->user_id = $request->auth_id;
            $user_result->correct_answer_count = $leagues;
            $user_result->random_counter_id = $request->random_id;
            $user_result->lang = app()->getLocale();
            $user_result->save();


            return $leagues;
        }

        if (app()->getLocale() == 'uz')
        {
            $resul2 =  Uzuseranswer::where('user_id','=',$request->auth_id)
                ->where('qive_answer_id','=',$request->savol_id)
                ->where('count_id','=',$request->random_id)->first();

            if ($resul2->answer_id != 0)
            {
                return 'Siz bu savolni oldin tanlab bo\'lgansiz ';
            }
            $resul2->answer_id = $request->message_pri;
            $resul2->update();

            $leagues = \DB::table('javobuzs')
                ->select('*')
                ->join('uz_users_answers', 'javobuzs.id', '=', 'uz_users_answers.answer_id')
                ->where('javobuzs.togri_javob', 1)
                ->where('uz_users_answers.user_id', Auth::user()->id)
                ->where('uz_users_answers.count_id', $request->random_id)
                ->count();


            $user_result = new Userresult();
            $user_result->user_id = $request->auth_id;
            $user_result->correct_answer_count = $leagues;
            $user_result->random_counter_id = $request->random_id;
            $user_result->lang = app()->getLocale();
            $user_result->save();


            return $leagues;
        }

        if (app()->getLocale() == 'ru')
        {
            $resul2 =  RUuseranswer::where('user_id','=',$request->auth_id)
                ->where('qive_answer_id','=',$request->savol_id)
                ->where('count_id','=',$request->random_id)->first();

            if ($resul2->answer_id != 0)
            {
                return 'Siz bu savolni oldin tanlab bo\'lgansiz ';
            }
            $resul2->answer_id = $request->message_pri;
            $resul2->update();

            $leagues = \DB::table('javobrus')
                ->select('*')
                ->join('ru_users_answers', 'javobrus.id', '=', 'ru_users_answers.answer_id')
                ->where('javobrus.togri_javob', 1)
                ->where('ru_users_answers.user_id', Auth::user()->id)
                ->where('ru_users_answers.count_id', $request->random_id)
                ->count();



            $user_result = new Userresult();
            $user_result->user_id = $request->auth_id;
            $user_result->correct_answer_count = $leagues;
            $user_result->random_counter_id = $request->random_id;
            $user_result->lang = app()->getLocale();
            $user_result->save();


            return $leagues;
        }

        if (app()->getLocale() == 'en')
        {
            $resul2 =  ENuseranswer::where('user_id','=',$request->auth_id)
                ->where('qive_answer_id','=',$request->savol_id)
                ->where('count_id','=',$request->random_id)->first();

            if ($resul2->answer_id != 0)
            {
                return 'Siz bu savolni oldin tanlab bo\'lgansiz ';
            }
            $resul2->answer_id = $request->message_pri;
            $resul2->update();

            $leagues = \DB::table('javobens')
                ->select('*')
                ->join('en_users_answers', 'javobens.id', '=', 'en_users_answers.answer_id')
                ->where('javobens.togri_javob', 1)
                ->where('en_users_answers.user_id', Auth::user()->id)
                ->where('en_users_answers.count_id', $request->random_id)
                ->count();

            $user_result = new Userresult();
            $user_result->user_id = $request->auth_id;
            $user_result->correct_answer_count = $leagues;
            $user_result->random_counter_id = $request->random_id;
            $user_result->lang = app()->getLocale();
            $user_result->save();


            return $leagues;
        }

    }

    public function get_update_answer_to(Request $request)
    {
        $answer = Javoboz::findorfail($request->category_id);
        $answer->javoblar = $request->javoblar;
        if (Input::get('correct_answer',true))
        {
            $answer->togri_javob = 1;
        }else
        {
            $answer->togri_javob = 0;
        }
        $answer->update();

    }

    public function archive_reviewer_quiz()
    {
        return view('reviewer.archive');
    }

}