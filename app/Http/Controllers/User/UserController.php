<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Model\Admin\Articles;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    public function register(){
        return view('register');
    }

    public function login(){
        return view('welcome');
    }

    public function post_login(Request $request)
    {
        $this->validate($request,[
            'email'=>'required',
            'password'=>'required',
        ]);

        if(Auth::attempt(['email'=>$request->email, 'password'=>$request->password,'status'=>'1']))
        {
            if (Auth::user()->role == 'admin')
            {
                return redirect()->route('admin.index');
            }
            if (Auth::user()->role == 'reviewer')
            {
                return redirect()->route('reviewer.dashboard');
                //return redirect()->route('reviewer.index');
            }
            if (Auth::user()->role == 'user')
            {
                return redirect()->route('reviewer.index');
            }
        }else{
            return redirect()->route('get.404');
        }
    }


    public function post_register(Request $request){
        $user = new User();
        $user->name = $request->name;
        $user->lastname = $request->lastname;
        $user->email = $request->email;
        $user->role = 'user';
        $user->status = 0;
        $user->password = bcrypt($request->password);
        $user->remember_token = $request['_token'];
        $user->save();
        return redirect('login');
    }

    public function user_index(){
        return view('user.index');
    }

    public function logout(){
        Auth::logout();
        return redirect()->route('get.login');
    }

    public function get_404()
    {
        return view('404');
    }
}
