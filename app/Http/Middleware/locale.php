<?php

namespace App\Http\Middleware;

use Closure;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

class locale
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $laravelLocale = LaravelLocalization::setLocale() ?: $this->app->config->get('app.fallback_locale');
        $locale = '/' . $laravelLocale;
        return $next($request);
    }
}
