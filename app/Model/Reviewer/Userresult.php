<?php

namespace App\Model\Reviewer;

use Illuminate\Database\Eloquent\Model;

class Userresult extends Model
{
    protected $table = 'users_and_results';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id', 'correct_answer_count', 'random_counter_id'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'remember_token',
    ];
}
