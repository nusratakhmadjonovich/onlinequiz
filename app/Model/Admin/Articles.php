<?php

namespace App\Model\Admin;

use Illuminate\Database\Eloquent\Model;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;


class Articles extends Model
{
    protected $fillable = [
        'title_uz',
        'title_uz-Latn',
        'title_en',
        'title_ru',
        'description_uz',
        'description_uz-Latn',
        'description_en',
        'description_ru',
        'photo',
        'status',
        'publish_at',
        'file',
        'author',
        'time',
        'slug',
    ];

    public function getTitleAttribute()
    {
        $title = 'title_' . LaravelLocalization::getCurrentLocale(); // here you obviously get the lang from wherever you need
        return $this->attributes[$title];
    }

    public function getDescriptionAttribute()
    {
        $title = 'description_' . LaravelLocalization::getCurrentLocale(); // here you obviously get the lang from wherever you need
        return $this->attributes[$title];
    }

}
