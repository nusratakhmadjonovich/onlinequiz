<?php
/**
 * Created by PhpStorm.
 * User: SysDev
 * Date: 30.04.2019
 * Time: 14:21
 */

namespace App\Libraries;


class Counter
{
    protected $counter;

    public function __construct() {
        $this->counter = 0;
    }

    public function incrementValue()
    {
        $this->counter++;
    }

    public function getValue() {
        return ++$this->counter;
    }

}