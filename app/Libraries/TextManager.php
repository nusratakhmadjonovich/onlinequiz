<?php
namespace App\Libraries;

use App\Models\Admin\Blocks;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

class TextManager
{
    public static function ShortText($string, $wordsreturned)
    {
        $retval = $string;
        $string = preg_replace('/(?<=\S,)(?=\S)/', ' ', $string);
        $string = str_replace("\n", " ", $string);
        $array = explode(" ", $string);
        if (count($array)<=$wordsreturned)
        {
            $retval = $string;
        }
        else
        {
            array_splice($array, $wordsreturned);
            $retval = implode(" ", $array)." ...";
        }
        return $retval;
    }
}
