<?php

namespace App\Libraries;

use Image;
use Illuminate\Support\Facades\Storage;

class UploadManager
{
    public static function upload($module, $object_id, $file, $old_file)
    {
        $storage = Storage::disk('public');
        $object_path = ($object_id ? "/" . $object_id : "");
        $new_file = sha1(time()) . '.' . $file->getClientOriginalExtension();
        $location = $module . "/" . $object_path;
        $storage->putFileAs($location, $file, $new_file);

        if ($old_file) {
            self::delete($module, $object_id, $old_file);
        }

        return $new_file;
    }

    public static function getFile($module, $object_id, $file)
    {
        $object_path = ($object_id ? "/" . $object_id : "");
        return Storage::url($module . $object_path . '/' . $file);
    }

    public static function delete($module, $object_id, $file)
    {
        $storage = Storage::disk('public');
        $object_path = ($object_id ? "/" . $object_id : "");
        $location = $module . $object_path;
        $location_with_image = $location . "/" . $file;

        if ($storage->exists($location_with_image)) {
            $storage->delete($location_with_image);
        }
        return true;
    }

    public static function uploadPhoto($module, $object_id, $photo, $old_photo)
    {
        $storage = Storage::disk('public');
        $params = config('image.upload_photos_params');
        $object_path = ($object_id ? "/" . $object_id : "");
        if (isset($params[$module])) {
            foreach ($params[$module] as $key => $param) {
                $file = sha1(time()) . '.' . $photo->getClientOriginalExtension();
                $location = $module . $object_path . "/" . $key;
                $location_with_image = $location . "/" . $file;
                if ($param) {
                    if ($param['type'] == 'fit') {
                        $resized_image = Image::make($photo)->fit($param['width'], $param['height'], function ($constraint) {
                            $constraint->upsize();
                        });
                    } elseif ($param['type'] == 'resize') {
                        $resized_image = Image::make($photo)->resize($param['width'], $param['height'], function ($constraint) {
                            $constraint->aspectRatio();
                            $constraint->upsize();
                        });
                    }
                    $storage->put($location_with_image, $resized_image->stream()->__toString(), 'public');
                } else {
                    $storage->putFileAs($location, $photo, $file);
                }
            }

            if ($old_photo) {
                self::deletePhoto($module, $object_id, $old_photo);
            }

            return $file;
        }
    }

    public static function getPhoto($module, $object_id, $size, $file)
    {
        $object_path = ($object_id ? "/" . $object_id : "");
        return Storage::url($module . $object_path . '/' . $size . '/' . $file);
    }

    public static function deletePhoto($module, $object_id, $file)
    {
        $storage = Storage::disk('public');
        $params = config('image.upload_photos_params');
        $object_path = ($object_id ? "/" . $object_id : "");
        if (isset($params[$module])) {
            foreach ($params[$module] as $key => $param) {
                $location = $module . $object_path . "/" . $key;
                $location_with_image = $location . "/" . $file;
                if ($storage->exists($location_with_image)) {
                    $storage->delete($location_with_image);
                }
            }
        }

        return true;
    }
}