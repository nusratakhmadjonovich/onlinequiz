<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFanlarsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fanlars', function (Blueprint $table) {
            $table->increments('id');
            $table->string('fanlar_name');
            $table->string('test_count')->nullable();
            $table->string('time')->nullable();
            $table->string('kiritilgan_vaqti');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fanlars');
    }
}
