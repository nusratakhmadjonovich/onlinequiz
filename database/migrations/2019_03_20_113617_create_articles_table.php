<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArticlesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('articles', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title_uz')->nullable();
            $table->string('title_uz-Latn')->nullable();
            $table->string('title_en')->nullable();
            $table->string('title_ru')->nullable();
            $table->integer('fanlar_id')->default(0);
            $table->longText('savol_uz')->nullable();
            $table->longText('savol_uz-Latn')->nullable();
            $table->longText('savol_en')->nullable();
            $table->longText('savol_ru')->nullable();
            $table->longText('description_ru')->nullable();
            $table->longText('description_en')->nullable();
            $table->longText('description_uz')->nullable();
            $table->longText('description_uz-Latn')->nullable();
            $table->string('photo')->nullable();
            $table->integer('status')->default(0);
            $table->string('publish_at')->nullable();
            $table->string('slug')->default(0);
            $table->string('file')->default(0);
            $table->integer('author')->nullable();
            $table->string('time')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('articles');
    }
}
