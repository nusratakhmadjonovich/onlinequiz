<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = new User();
        $admin->name = 'admin';
        $admin->lastname = 'admin';
        $admin->email = 'admin@gmail.com';
        $admin->password = bcrypt('admin');
        $admin->role = 'admin';
        $admin->remember_token = bcrypt('admin');
        $admin->save();

        $reviewer = new User();
        $reviewer->name = 'reviewer';
        $reviewer->lastname = 'reviewer';
        $reviewer->email = 'reviewer@gmail.com';
        $reviewer->password = bcrypt('reviewer');
        $reviewer->role = 'reviewer';
        $reviewer->remember_token = bcrypt('reviewer');
        $reviewer->save();

        $user = new User();
        $user->name = 'user';
        $user->lastname = 'user';
        $user->email = 'user';
        $user->password = bcrypt('user');
        $user->role = 'user';
        $user->remember_token = bcrypt('user');
        $user->save();
    }
}
