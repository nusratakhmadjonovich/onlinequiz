<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" type="text/css" href="http://www.mercurysolutions.co/app/webroot/css/common/bootstrap.min.css"/>
    <style>
        .hide{
            display:none;
        }
    </style>
</head>
<body>

<?php
$qu = array(
    array('id'=>'1','q'=>"A",'a'=>"A1",'b'=>"A2"),
    array('id'=>'2','q'=>"B",'a'=>"B1",'b'=>"B2"),
    array('id'=>'3','q'=>"C",'a'=>"C1",'b'=>"C2"),
    array('id'=>'4','q'=>"D",'a'=>"D1",'b'=>"D2"),
    array('id'=>'5','q'=>"E",'a'=>"E1",'b'=>"E2")
);$i=1;
echo '
    <div class="questions" id="question-div">
        <form action="" method="POST" id="question-form">';foreach($qu as $r){echo '
            <div align="center" id="div-'.$i.'" class="question'; if($i>1)echo ' hide';echo '">
                <p>Question '.$i.' : <input type="hidden" name="question[]" value="'.$r['id'].'" id="'.$r['id'].'" />'.$r['q'].'</p>
                <label class="radio-inline" data-id="'.$i.'" ><input type="radio" required name="a-'.$r['id'].'" value="'.$r['a'].'">'.$r['a'].'</label>
                <label class="radio-inline" data-id="'.$i.'" ><input type="radio" required name="a-'.$r['id'].'" value="'.$r['b'].'">'.$r['b'].'</label><hr />
            </div>';$i++;
}echo '
            <div class="button hide" id="next">Next</div>
            <div class="button hide" id="prev">Prev</div>
            <button type="submit" id="submit" class="btn btn-primary btn-sm pull-right  hide">Submit</button>
        </form>
    </div>';?>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script type="text/javascript">
    var maxq = 6;
    $('.radio-inline').click(function(e) {
        var id = parseInt($(this).data('id'));
        if(id==1) $('.button').addClass('hide');
        if(id!=(maxq-1)){$('#next').removeClass('hide');}
        var next = (id+1);
        var prev = (id-1);
        $('#next').data('id',next);
        $('#prev').data('id',prev);
    });
    $('#next').click(function(e) {
        var id = $(this).data('id');
        $('.button').addClass('hide');
        //$('#next').removeClass('hide');
        if(id==(maxq-1)) {$('#submit,#prev').removeClass('hide');}
        else {$('.button').addClass('hide');$('#prev').removeClass('hide');}
        $('.question').addClass('hide');
        $('#div-'+id).removeClass('hide');
        var next = id+1;
        var prev = id-1;
        $('#next').data('id',next);
        $('#prev').data('id',prev);
    });
    $('#prev').click(function(e) {
        var id = $(this).data('id');
        $('#prev').removeClass('hide');
        if(id==1)$('.button').addClass('hide');
        $('.question').addClass('hide');
        $('#div-'+id).removeClass('hide');
        var next = id+1;
        var prev = id-1;
        $('#next').data('id',next);
        $('#prev').data('id',prev);
    });
</script>
</body>
</html>
