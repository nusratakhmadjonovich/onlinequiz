@extends('layouts.admin ')

{{--@section('header_title')
    <h1 class="text-center" ">Fanlar ro`yxati</h1>
@stop--}}

@section('content')
    <div class="col-md-12 ">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Fanlar ro`yxati</h3>
                <a href="{{ url('admin/fanlar/creat') }}" class="btn btn-success pull-right btn-sm" >Creat</a>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <table id="example1" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th>Catecogy Name</th>
                        <th>Yaratilgan sanasi</th>
                        <th>Berilgan Vaqt</th>
                        <th>Testlar soni</th>
                        <th>Qo`shimcha</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($name as $item)
                        <tr>
                            <td>{{ $item->fanlar_name }}</td>
                            <td>{{ $item->kiritilgan_vaqti }}</td>
                            <td>{{ $item->time }}</td>
                            <td>{{ $item->test_count }}</td>
                            <td>
                                <a href="{{--{{ url('admin/categories/show',['id'=>$item->id]) }}--}}" class="btn btn-info btn-sm"><i class="glyphicon glyphicon-eye-open"></i></a>
                                <a href="{{ url('admin/fanlar/edit',['id'=>$item->id]) }}" class="btn btn-success btn-sm"><i class="	glyphicon glyphicon-edit"></i></a>
                                <a href="{{ url('admin/fanlar/delete',['id'=>$item->id]) }}" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
@stop