@extends('layouts.admin')

@section('title_header')
    Edit
@stop

@section('content')
    <section class="content">
        <div class="row">
            <div class="col-md-offset-1 col-md-10">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Edit Category</h3>
                        <a href="{{ url('/admin/fanlar') }}" class="btn btn-default pull-right">Back</a>
                    </div>
                    <form role="form" action="{{ route('post.fanlar.update',['id'=>$fanlar->id]) }}" method="POST">
                        {{csrf_field()}}
                        <div class="box-body">
                            <div class="form-group">
                                <label for="name">Category name</label>
                                <input type="name" class="form-control" value="{{ $fanlar->fanlar_name }}" name="name" id="name" placeholder="Enter category">
                            </div>
                            <div class="form-group">
                                <label for="time">Time</label>
                                <input type="text" class="form-control" name="time" id="time" value="{{ $fanlar->time }}" placeholder="time">
                            </div>
                            <div class="form-group">
                                <label for="test_count">Testlar soni</label>
                                <input type="text" class="form-control" name="test_count" value="{{ $fanlar->test_count }}" id="test_count" placeholder="testlar soni">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Publish_at</label>
                                <input type="text" class="form-control" value="{{ $fanlar->kiritilgan_vaqti }}" name="vaqti" id="exampleInputPassword1" placeholder="publish at">
                            </div>
                        </div>
                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>
                </div>
            </div>

        </div>
        <!-- /.row -->
    </section>
@stop