@extends('layouts.admin')
@section('content')
<section class="content">
    <div class="row">
        <div class="col-md-offset-1 col-md-10">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Quick Example</h3>
                    <a href="{{ url('/admin/fanlar') }}" class="btn btn-default pull-right">Back</a>
                </div>
                <form role="form" action="{{ route('post.creat') }}" method="POST">
                    {{csrf_field()}}
                    <div class="box-body">
                        <div class="form-group">
                            <label for="name">Fan nomini kiriting:</label>
                            <input type="name" class="form-control" name="name" id="name" placeholder="Enter category">
                        </div>
                        <div class="form-group">
                            <label for="time">Time</label>
                            <input type="text" class="form-control" name="time" id="time" placeholder="time">
                        </div>
                        <div class="form-group">
                            <label for="test_count">Testlar soni</label>
                            <input type="text" class="form-control" name="test_count" id="test_count" placeholder="testlar soni">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Kiritilgan vaqti:</label>
                            <input type="date" class="form-control"name="vaqti" id="exampleInputPassword1" placeholder="vaqti">
                        </div>
                    </div>
                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- /.row -->
</section>
@stop