@extends('layouts.admin')

@section('title') Страницы @stop
@section('title-child') @if($isNewRecord) Создать страница @else Редактировать @endif @stop

@section('content')
    <div class="row">

        <form method="POST" action="{{ url('/admin/articles/'.$post->id) }}" accept-charset="UTF-8" enctype="multipart/form-data">

            <div class="col-xs-8">
                <div class="box"  style="margin-left: 15px;">
                    <div class="box-body">
                        <h4>@if($isNewRecord) Создать страница @else Редактировать @endif</h4>
                        {{ method_field('PATCH') }}
                        {{ csrf_field() }}
                        <div class="form-group col-md-12">
                                @if(Session::has('success-message'))
                                <div class="alert alert-success alert-dismissible">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                    <h4><i class="icon fa fa-check"></i> Alert!</h4>
                                    {{ Session::get('success-message') }}
                                </div>
                            @endif
                        </div>
                        <div class="form-group col-md-12">
                            {{--<h3>Урл: page/{{$post->id}}</h3>--}}
                            <div class="nav-tabs-custom">
                                <ul class="nav nav-tabs" id="myTab">
                                    <li class="active {{$errors->has('title_ru') ? 'required' : '' }}"><a href="#tab_1" data-toggle="tab" aria-expanded="true" style="{{ $errors->has('name_ru') ? 'color:#a94442;' : ''}}" >Русские</a></li>
                                    <li class="{{$errors->has('title_en') ? 'required' : '' }}"><a href="#tab_2" data-toggle="tab" aria-expanded="true" style="{{ $errors->has('name_ru') ? 'color:#a94442;' : ''}}">English</a></li>
                                    <li class="{{$errors->has('title_uz') ? 'required' : '' }}"><a href="#tab_3" data-toggle="tab" aria-expanded="true" style="{{ $errors->has('name_ru') ? 'color:#a94442;' : ''}}">Ўзбек</a></li>
                                    <li class="{{$errors->has('title_uz-Latn') ? 'required' : '' }}"><a href="#tab_4" data-toggle="tab" aria-expanded="false" style="{{ $errors->has('name_ru') ? 'color:#a94442;' : ''}}">O'zbek</a></li>
                                </ul>
                                <div class="tab-content">
                                    <div class="tab-pane active" id="tab_1">
                                        <div class="{{ $errors->has('title_ru') ? 'has-error' : ''}}">
                                            <label for="title_ru" class="control-label required">{{ 'Название' }} </label>
                                            <input class="form-control" name="title_ru" type="text" id="title_ru" value="{{ old('title_ru', $post->title_ru)}}">
                                            {!! $errors->first('title_ru', '<p class="error-block">:message</p>') !!}
                                        </div>

                                        <div class="{{ $errors->has('description_ru') ? 'has-error' : ''}}">
                                            <label for="description_ru" class="control-label">{{ 'Описание' }}</label>
                                            <textarea name="description_ru" id="editor1" class="form-control">{{ old('description_ru', $post->description_ru) }}</textarea>
                                            {!! $errors->first('description_ru', '<p class="error-block">:message</p>') !!}
                                        </div>
                                        <br>


                                        <div class="containercha1" >
                                            <div class='element1' id='divcha_1'>
                                                <div class="entry input-group col-xs-12">
                                                    <span class="input-group-addon">
                                                        <input type="checkbox">
                                                    </span>
                                                     <input class="form-control" name="javob[ru][1]" type='text' placeholder='Enter your skill'  >
                                                     <span class="input-group-btn birinchi">
                                                        <button class="btn btn-success " type="button">
                                                            <span class="glyphicon glyphicon-plus"></span>
                                                        </button>
                                                     </span>
                                                </div>
                                                @foreach($javob_ru as $key=>$item)
                                                    <p>
                                                    <div class="col-md-3">
                                                        <input type="checkbox" name="radio_button" class="check_correct_answer" @if($item->togri_javob == 1) checked @endif>
                                                    </div>
                                                    <div class="col-md-6" class="check_correct_text">
                                                        {{ $item->javoblar }}
                                                    </div>
                                                    <div class="col-md-3">
                                                        <button type="button" data-cor="{{ $item->togri_javob }}" data-quesid="{{ $post->id }}" data-catid="{{ $item->id }}" data-lang="ru"  data-javob="{{ $item->javoblar }}" class="pull-left btn btn-success btn-xs" data-toggle="modal" data-target="#exampleModal">
                                                            <i class="glyphicon glyphicon-edit"></i>
                                                        </button>
                                                        <button type="button" data-trashid="{{ $item->id }}" data-lang="ru" data-quesid="{{ $post->id }}" class="myBtn pull-right btn btn-danger btn-xs" data-toggle="modal" data-target="#myModal">
                                                            <i class="glyphicon glyphicon-trash"></i>
                                                        </button>
                                                    </div>
                                                    </p>
                                                    <br>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>


                                    <div class="tab-pane" id="tab_2">
                                        <div class="{{ $errors->has('title_en') ? 'has-error' : ''}}">
                                            <label for="title_en" class="control-label required">{{ 'Title' }}</label>
                                            <input class="form-control" name="title_en" type="text" id="title_en" value="{{ old('title_en', $post->title_en) }}">
                                            {!! $errors->first('title_en', '<p class="error-block">:message</p>') !!}
                                        </div>

                                        <div class="{{ $errors->has('description_en') ? 'has-error' : ''}}">
                                            <label for="description_en" class="control-label">{{ 'Description' }}</label>
                                            <textarea name="description_en" id="editor2" class="form-control">{{ old('description_en', $post->description_en) }}</textarea>
                                            {!! $errors->first('description_en', '<p class="error-block">:message</p>') !!}
                                        </div>
                                        <br>
                                        <div class="containercha2" >
                                            <div class='element2' id='divcha2_1'>
                                                <div class="entry input-group col-xs-12">
                                                    <span class="input-group-addon">
                                                        <input type="checkbox">
                                                    </span>
                                                    <input class="form-control" name="javob[en][1]" type='text' placeholder='Enter your skill'  >
                                                    <span class="input-group-btn ikkinchi">
                                                        <button class="btn btn-success " type="button">
                                                            <span class="glyphicon glyphicon-plus"></span>
                                                        </button>
                                                     </span>
                                                </div>
                                                @foreach($javob_en as $key=>$item)
                                                    <p>
                                                    <div class="col-md-3">
                                                        <input type="checkbox" name="radio_button" class="check_correct_answer" @if($item->togri_javob == 1) checked @endif>
                                                    </div>
                                                    <div class="col-md-6" class="check_correct_text">
                                                        {{ $item->javoblar }}
                                                    </div>
                                                    <div class="col-md-3">
                                                        <button type="button" data-cor="{{ $item->togri_javob }}" data-quesid="{{ $post->id }}" data-catid="{{ $item->id }}" data-lang="en"  data-javob="{{ $item->javoblar }}" class="pull-left btn btn-success btn-xs" data-toggle="modal" data-target="#exampleModal">
                                                            <i class="glyphicon glyphicon-edit"></i>
                                                        </button>
                                                        <button type="button" data-trashid="{{ $item->id }}" data-lang="en" data-quesid="{{ $post->id }}" class="myBtn pull-right btn btn-danger btn-xs" data-toggle="modal" data-target="#myModal">
                                                            <i class="glyphicon glyphicon-trash"></i>
                                                        </button>
                                                    </div>
                                                    </p>
                                                    <br>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="tab_3">
                                        <div class="{{ $errors->has('title_uz') ? 'has-error' : ''}}">
                                            <label for="title_uz" class="control-label required">{{ 'Сарлавҳа' }}</label>
                                            <input class="form-control" name="title_uz" type="text" id="title_uz" value="{{ old('title_uz', $post->title_uz) }}">
                                            {!! $errors->first('title_uz', '<p class="error-block">:message</p>') !!}
                                        </div>

                                        <div class="{{ $errors->has('description_uz') ? 'has-error' : ''}}">
                                            <label for="description_uz" class="control-label">{{ 'Матн' }}</label>
                                            <textarea name="description_uz" id="editor3" class="form-control">{{ old('description_uz', $post->description_uz) }}</textarea>
                                            {!! $errors->first('description_uz', '<p class="error-block">:message</p>') !!}
                                        </div>
                                        <br>
                                        <div class="containercha3" >
                                            <div class='element3' id='divcha3_1'>
                                                <div class="entry input-group col-xs-12">
                                                    <span class="input-group-addon">
                                                        <input type="checkbox">
                                                    </span>
                                                    <input class="form-control" name="javob[uz][1]" type='text' placeholder='Enter your skill'  >
                                                    <span class="input-group-btn uchinchi">
                                                        <button class="btn btn-success " type="button">
                                                            <span class="glyphicon glyphicon-plus"></span>
                                                        </button>
                                                     </span>
                                                </div>
                                                @foreach($javob_uz as $key=>$item)
                                                    <p>
                                                    <div class="col-md-3">
                                                        <input type="checkbox" name="radio_button" class="check_correct_answer" @if($item->togri_javob == 1) checked @endif>
                                                    </div>
                                                    <div class="col-md-6" class="check_correct_text">
                                                        {{ $item->javoblar }}
                                                    </div>
                                                    <div class="col-md-3">
                                                        <button type="button" data-cor="{{ $item->togri_javob }}" data-quesid="{{ $post->id }}" data-catid="{{ $item->id }}" data-lang="uz"  data-javob="{{ $item->javoblar }}" class="pull-left btn btn-success btn-xs" data-toggle="modal" data-target="#exampleModal">
                                                            <i class="glyphicon glyphicon-edit"></i>
                                                        </button>
                                                        <button type="button" data-trashid="{{ $item->id }}" data-quesid="{{ $post->id }}" data-lang="uz" class="myBtn pull-right btn btn-danger btn-xs" data-toggle="modal" data-target="#myModal">
                                                            <i class="glyphicon glyphicon-trash"></i>
                                                        </button>
                                                    </div>
                                                    </p>
                                                    <br>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="tab_4">
                                        <div class="{{ $errors->has('title_uz-Latn') ? 'has-error' : ''}}">
                                            <label for="title_uz-Latn" class="control-label required">{{ 'Sarlavha' }}</label>
                                            <input class="form-control" name="title_uz-Latn" type="text" id="title_uz-Latn" value="{{ old('title_uz-Latn',$post['title_uz-Latn']) }}">
                                            {!! $errors->first('title_uz-Latn', '<p class="error-block">:message</p>') !!}
                                        </div>

                                        <div class="{{ $errors->has('description_uz-Latn') ? 'has-error' : ''}}">
                                            <label for="description_uz-Latn" class="control-label">{{ 'Matn' }}</label>
                                            <textarea name="description_uz-Latn" id="editor4" class="form-control">{{ old('description_uz-Latn', $post['description_uz-Latn']) }}</textarea>
                                            {!! $errors->first('description_uz-Latn', '<p class="error-block">:message</p>') !!}
                                        </div>
                                        <br>
                                        <div class="containercha" >
                                            <div class='element' id='div_1'>
                                                <div class="entry input-group col-xs-12">
                                                    <span class="input-group-addon">
                                                        <input type="checkbox">
                                                    </span>
                                                    <input class="form-control"  name="javob[oz][1]"  type='text' value="" placeholder='Enter your skill' >
                                                    <span class="input-group-btn add">
                                                        <button class="btn btn-success " type="button">
                                                            <span class="glyphicon glyphicon-plus"></span>
                                                        </button>
                                                     </span>
                                                </div>
                                                @foreach($javob_oz as $key=>$item)
                                                        <p>
                                                        <div class="col-md-3">
                                                            <input type="checkbox" name="radio_button" class="check_correct_answer" @if($item->togri_javob == 1) checked @endif>
                                                        </div>
                                                        <div class="col-md-6" class="check_correct_text">
                                                            {{ $item->javoblar }}
                                                        </div>
                                                        <div class="col-md-3">
                                                            <button type="button" data-cor="{{ $item->togri_javob }}" data-quesid="{{ $post->id }}" data-catid="{{ $item->id }}" data-lang="oz"  data-javob="{{ $item->javoblar }}" class="pull-left btn btn-success btn-xs" data-toggle="modal" data-target="#exampleModal">
                                                                <i class="glyphicon glyphicon-edit"></i>
                                                            </button>
                                                            <button type="button" data-trashid="{{ $item->id }}" data-lang="oz" data-quesid="{{ $post->id }}" class="myBtn pull-right btn btn-danger btn-xs" data-toggle="modal" data-target="#myModal">
                                                                <i class="glyphicon glyphicon-trash"></i>
                                                            </button>
                                                        </div>
                                                        </p>
                                                        <br>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-4">
                <div class="box">
                    <div class="box-header">
                    </div>
                    <div class="box-body">
                        <div class="col-md-12">
                            <button type="submit" class="btn btn-success"><i class="fa fa-check"></i> Сохранить </button>
                            <a href="{{ url('/admin/articles') }}" class="btn btn-default pull-right"><span class="fa fa-times-circle"></span> Cancel</a>
                        </div>
                        <br>
                        <br>
                        <div class="form-group col-md-12 {{ $errors->has('publish_at') ? 'has-error' : ''}}">
                            <label for="publish_at" class="control-label">{{ 'Дата публикации' }}</label>
                            <div class="input-group">
                                <input type="date" id="publish_at" class="form-control datetimepicker" name="publish_at" value="{{ old('publish_at', $post->publish_at ? $post->publish_at : date('Y-m-d H:i')) }}">
                                <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                            </div>
                            {!! $errors->first('publish_at','<p class="error-block">:message</p>') !!}
                        </div>
                        <div class="form-group col-md-12">
                            <label for="article_category_id" class="control-label">{{ 'Рубрика' }}</label>
                            <select id="article_category_id" class="form-control select2"  name="article_category_id">
                                @foreach($fanlar as $fan)
                                    <option value="{{$fan->id}}" @if($fan->id == $post->fanlar_id) selected @endif>{{$fan->fanlar_name}}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group col-md-12 {{ $errors->has('file') ? 'has-error' : ''}}">
                            <label for="file" class="control-label">{{ 'файл' }}</label>
                            <div class="input-group">
                                <div class="input-group-addon"><i class="fa fa-file-archive-o"></i></div>
                                <input class="form-control" name="file" type="file" id="file" value="{{ $post->file or ''}}">
                            </div>
                            {!! $errors->first('file', '<p class="error-block">:message</p>') !!}
                            <br>
                            @if($post->file)
                                <a href="{{ App\Libraries\UploadManager::getFile('files', $post->id, $post->file)}}" target="_blank"> {{$post->file}} </a>
                                <a href="{{ url('admin/articles/removeFile/'.$post->id) }}" >удалить файл</a>
                            @endif
                        </div>

                        <div class="form-group col-md-12 {{ $errors->has('time') ? 'has-error' : ''}}">
                            <label for="file" class="control-label">{{ 'Время' }}</label>
                            <div class="input-group">
                                <div class="input-group-addon"><i class="fa fa-clock-o"></i></div>
                                <input class="form-control" name="time" type="text" id="time" value="{{ $post->time or ''}}" placeholder="Время">
                            </div>
                            {!! $errors->first('time', '<p class="error-block">:message</p>') !!}
                            <br>
                        </div>

                        <div class="form-group col-md-12 {{ $errors->has('status') ? 'has-error' : ''}}">
                            <label for="status" class="control-label">{{ 'Статус' }}</label>
                            <div class="input-group">
                                <div class="input-group-addon"><i class="fa fa-handshake-o"></i></div>
                                <input class="form-control" name="status" type="text" id="status" value="{{ $post->status or ''}}" placeholder="Статус">
                            </div>
                            {!! $errors->first('status', '<p class="error-block">:message</p>') !!}
                            <br>
                        </div>

                        <div class="form-group col-md-12 {{ $errors->has('photo') ? 'has-error' : ''}}">
                            <label for="photo" class="control-label">{{ 'фото' }}</label>
                            <div class="input-group">
                                <div class="input-group-addon"><i class="fa fa-file-image-o"></i></div>
                                <input class="form-control" name="photo" type="file" id="photo" value="{{ $post->photo or ''}}">
                            </div>
                            {!! $errors->first('photo', '<p class="error-block">:message</p>') !!}
                            <div class="img-input-show">
                                <br>
                                @if($post->photo)
                                    <img src="{{ App\Libraries\UploadManager::getPhoto('app/public/article', $post->id, '200x200', $post->photo)}}" class="img-thumbnail" width="150"/>
                                    <a href="{{ url('admin/articles/removePhoto/'.$post->id) }}"  >удалить фото</a>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>




    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="{{ route('post.answers.question','test') }}" id="modal_body" method="post">
                    {{ method_field('patch') }}
                    {{ csrf_field() }}
                <div class="modal-body" >
                        <input type="hidden" name="category_id" id="cat_id" value="">
                        <input type="hidden" name="ques_id" id="ques_id" value="">
                        <input type="hidden" name="lang" id="lang" value="">
                        <div class="form-group" id="title">

                        </div>
                        <br>
                        <div class="form-group">
                            <input type="text" name="javoblar" class="form-control" id="desc" value="">
                        </div>
                    <div class="form-group">
                            <input type="text" name="correct_answer" class="form-control" id="correct_answer" value="">
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-success" {{--id="save_changes_model"--}}>Save changes</button>
                </div>
                </form>
            </div>
        </div>
    </div>


    <!-- Trash Modal -->

    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <form action="{{ route('delete.answer.from.question') }}" method="post">
                {{ csrf_field() }}
                <div class="modal-content">
                    <div class="modal-header" style="background-color: red; text-align: center;font-weight: bold;color: white">
                        <button type="button" class="close" data-dismiss="modal"> &times; </button>
                        <h4 class="modal-title">Trash asnwer from question </h4>
                    </div>
                    <div class="modal-body">
                        <input type="hidden" id="ques_id" name="ques_id">
                        <input type="hidden" id="trashid" name="trashid">
                        <input type="hidden" id="lang" name="lang">
                        <p style="font-weight: bold;text-align: center; padding-top: 15px;">Are you shure delete this answer from question ?</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-success pull-left" data-dismiss="modal"><i class="glyphicon glyphicon-backward" style="padding-right: 15px;"></i>No</button>
                        <button type="submit" class="btn btn-danger pull-right"> Trash <i class="glyphicon glyphicon-forward" style="padding-left: 15px;"></i></button>
                    </div>
                </div>
            </form>

        </div>
    </div>

    </div>
@endsection

@section('scripts')
    <script>
        $(document).ready(function() {
            if (location.hash) {
                $("a[href='" + location.hash + "']").tab("show");
            }
            $(document.body).on("click", "a[data-toggle]", function(event) {
                location.hash = this.getAttribute("href");
            });
        });
        $(window).on("popstate", function() {
            var anchor = location.hash || $("a[data-toggle='tab']").first().attr("href");
            $("a[href='" + anchor + "']").tab("show");
        });
    </script>
    <script language="javascript">
        $(document).ready(function(){

            // on modal show
            $('#exampleModal').on('show.bs.modal', function(event) {

                var button = $(event.relatedTarget);
                var answer = button.data('cor');
                var javob = button.data('javob');
                var cat_id = button.data('catid');
                var ques_id = button.data('quesid');
                var lang = button.data('lang');
                var trashid = button.data('trashid');
                var modal = $(this);

                modal.find('.modal-body #desc').attr('value',javob);
                modal.find('.modal-body #ques_id').attr('value',ques_id);
                modal.find('.modal-body #lang').attr('value',lang);
                modal.find('.modal-body #correct_answer').attr('value',answer);
                modal.find('.modal-body #cat_id').attr('value',cat_id);
                modal.find('.modal-body #trashid').attr('value',trashid);


            });

            $('#myModal').on('show.bs.modal', function(event) {

                var button = $(event.relatedTarget);
                var trashid = button.data('trashid');
                var lang = button.data('lang');
                var ques_id = button.data('quesid');
                var modal = $(this);
                modal.find('.modal-body #ques_id').attr('value',ques_id);
                modal.find('.modal-body #lang').attr('value',lang);
                modal.find('.modal-body #trashid').attr('value',trashid);

            });

            $('#trash_exampleModal').on('show.bs.trash_modal', function(event) {

                var button = $(event.relatedTarget);
                var answer = button.data('cor');
                var javob = button.data('javob');
                var cat_id = button.data('catid');
                var ques_id = button.data('quesid');
                var trashid = button.data('trashid');
                var modal = $(this);

                modal.find('.modal-body #desc').attr('value',javob);
                modal.find('.modal-body #ques_id').attr('value',ques_id);
                modal.find('.modal-body #correct_answer').attr('value',answer);
                modal.find('.modal-body #cat_id').attr('value',cat_id);
                modal.find('.modal-body #trashid').attr('value',trashid);


            });


            $('#save_changes_model').click(function (stay) {
                stay.preventDefault();
                alert('it is cliked !');
                var form = $('#modal_body').serialize();
                $.ajax( {
                    type: "POST",
                    url: '{{ url('/reviewer/update/get') }}',
                    data: form,
                    success: function( response ) {
                        alert('success => '+response);
                        console.log( response );
                    },
                    error:function (response) {
                        console.log(response);
                        alert('error => '+response);
                    }
                } );


            });

            // Add new element
            $(".birinchi").click(function(){
                // Finding total number of elements added
                var total_element1 = $(".element1").length;
                // last <div> with element class id
                var lastid1 = $(".element1:last").attr("id");
                var split_id1 = lastid1.split("_");
                var nextindex = Number(split_id1[1]) + 1;
                var max = 15;
                // Check total number elements
                if(total_element1 < max ){
                    // Adding new div container after last occurance of element class
                    $(".element1:last").after("<div class='element1' id='divcha_"+ nextindex +"'></div>");
                    // Adding element to <div>
                    $("#divcha_" + nextindex).append("<br><div class=\"entry input-group col-xs-12\"><span class=\"input-group-addon\"><input type=\"checkbox\"></span><input class=\"form-control\" name='javob[ru]["+nextindex+"]' type='text' placeholder='Enter your skill'  id='txt_"+ nextindex +"'><span id='remove1_" + nextindex + "' class='input-group-btn remove1'><button class=\"btn btn-danger \" type=\"button\">\n" + "<span class=\"glyphicon glyphicon-minus\" ></span>\n" + "</button></span></div>");
                }
            });
            // Remove element
            $('.containercha1').on('click','.remove1',function(){

                var id = this.id;
                var split_id1 = id.split("_");
                var deleteindex1 = split_id1[1];

                // Remove <div> with id
                $("#divcha_" + deleteindex1).remove();
            });
        });
    </script>
    <SCRIPT language="javascript">
        $(document).ready(function(){
            // Add new element
            $(".ikkinchi").click(function(){
                // Finding total number of elements added
                var total_element2 = $(".element2").length;
                // last <div> with element class id
                var lastid2 = $(".element2:last").attr("id");
                var split_id2 = lastid2.split("_");
                var nextindex = Number(split_id2[1]) + 1;
                var max = 15;
                // Check total number elements
                if(total_element2 < max ){
                    // Adding new div container after last occurance of element class
                    $(".element2:last").after("<div class='element2' id='divcha2_"+ nextindex +"'></div>");
                    // Adding element to <div>
                    $("#divcha2_" + nextindex).append("<br><div class=\"entry input-group col-xs-12\"><span class=\"input-group-addon\"><input type=\"checkbox\"></span><input class=\"form-control\" name='javob[en]["+nextindex+"]' type='text' placeholder='Enter your skill'  id='txt_"+ nextindex +"'><span id='remove2_" + nextindex + "' class='input-group-btn remove2'><button class=\"btn btn-danger \" type=\"button\">\n" + "<span class=\"glyphicon glyphicon-minus\" ></span>\n" + "</button></span></div>");
                }
            });
            // Remove element
            $('.containercha2').on('click','.remove2',function(){

                var id = this.id;
                var split_id2 = id.split("_");
                var deleteindex2 = split_id2[1];

                // Remove <div> with id
                $("#divcha2_" + deleteindex2).remove();
            });
        });
    </SCRIPT>
    <SCRIPT language="javascript">
        $(document).ready(function(){
            // Add new element
            $(".uchinchi").click(function(){
                // Finding total number of elements added
                var total_element3 = $(".element3").length;
                // last <div> with element class id
                var lastid3 = $(".element3:last").attr("id");
                var split_id3 = lastid3.split("_");
                var nextindex = Number(split_id3[1]) + 1;
                var max = 15;
                // Check total number elements
                if(total_element3 < max ){
                    // Adding new div container after last occurance of element class
                    $(".element3:last").after("<div class='element3' id='divcha3_"+ nextindex +"'></div>");
                    // Adding element to <div>
                    $("#divcha3_" + nextindex).append("<br><div class=\"entry input-group col-xs-12\"><span class=\"input-group-addon\"><input type=\"checkbox\"></span><input class=\"form-control\" name='javob[uz]["+nextindex+"]' type='text' placeholder='Enter your skill'  id='txt_"+ nextindex +"'><span id='remove3_" + nextindex + "' class='input-group-btn remove3'><button class=\"btn btn-danger \" type=\"button\">\n" + "<span class=\"glyphicon glyphicon-minus\" ></span>\n" + "</button></span></div>");
                }
            });
            // Remove element
            $('.containercha3').on('click','.remove3',function(){

                var id = this.id;
                var split_id3 = id.split("_");
                var deleteindex3 = split_id3[1];

                // Remove <div> with id
                $("#divcha3_" + deleteindex3).remove();
            });
        });
    </SCRIPT>
    <SCRIPT language="javascript">
        $(document).ready(function(){
            // Add new element
            $(".add").click(function(){
                // Finding total number of elements added
                var total_element = $(".element").length;
                // last <div> with element class id
                var lastid = $(".element:last").attr("id");
                var split_id = lastid.split("_");
                var nextindex = Number(split_id[1]) + 1;
                var max = 15;
                // Check total number elements
                if(total_element < max ){
                    // Adding new div container after last occurance of element class
                    $(".element:last").after("<div class='element' id='div_"+ nextindex +"'></div>");
                    // Adding element to <div>
                    $("#div_" + nextindex).append("<br><div class=\"entry input-group col-xs-12\"><span class=\"input-group-addon\"><input type=\"checkbox\"></span><input class=\"form-control\"  name='javob[oz]["+nextindex+"]' type='text' placeholder='Enter your skill'  id='txt_"+ nextindex +"'><span id='remove_" + nextindex + "' class='input-group-btn remove'><button class=\"btn btn-danger \" type=\"button\">\n" + "<span class=\"glyphicon glyphicon-minus\" ></span>\n" + "</button></span></div>"+"<input type='hidden' name='ss' value='"+nextindex+"'>");

                }
            });
            // Remove element
            $('.containercha').on('click','.remove',function(){

                var id = this.id;
                var split_id = id.split("_");
                var deleteindex = split_id[1];

                // Remove <div> with id
                $("#div_" + deleteindex).remove();
            });
        });
    </SCRIPT>
    <script src="//cdn.ckeditor.com/4.6.2/full-all/ckeditor.js"></script>
    <script src="{{asset('js/lckeditor.js')}}"></script>
    <script src="{{ asset('js/ckeditor_options.js') }}"></script>
    <script>
        CKEDITOR.replace('description_ru', options);
        CKEDITOR.replace('description_en', options);
        CKEDITOR.replace('description_uz', options);
        CKEDITOR.replace('description_uz-Latn', options);
    </script>
@stop