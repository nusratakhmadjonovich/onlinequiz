@extends('layouts.admin')

@section('title')
    Articles
@stop

@section('content')

        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <a href="{{ url('/admin/articles/create') }}" class="btn btn-success pull-right">
                        <i class="fa fa-plus" aria-hidden="true"></i> Добавить
                    </a>
                </div>

                {{--@include('includes.messages') @include('includes.error_messages')--}}
                <div class="box-body">
                    <div class="search-box">
                        <form method="POST" id="search-form" class="form-inline" role="form">

                            <div class="form-group">
                                <input type="text" class="form-control" name="title_uz-Latn" id="user-table" placeholder="Загаловок">
                            </div>
                            <button type="submit" class="btn btn-primary">Поиск</button>
                        </form>
                    </div>

                    <br>

                    <div class="table-responsive">
                        <table class="table table-bordered" id="users-table">
                            <thead>
                            <tr>
                                <th width="20">#</th>
                                <th width="240">Загаловок</th>
                                <th width="80">Дуйствие</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->

@section('scripts')
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        var oTable = $('#users-table').DataTable({
            processing: true,
            serverSide: true,
            bLengthChange: false,
            bFilter: false,
            ajax: {
                url: "{{  route('admin.articles.list') }}",
                method: 'GET',
                data: function (d) {
                    var filter_data = $('#search-form').serializeArray();
                    $.each(filter_data, function(key, item) {
                        d[item.name] = item.value;
                    });
                }
            },
            columns: [
                {data: 'id', name: 'id'},
                {data:'title_uz-Latn',name:'title_uz-Latn'},
                {data: 'actions', name: 'actions', orderable: false}
            ]
        });

        $('#search-form').on('submit', function(e) {
            oTable.draw();
            e.preventDefault();
        });
    </script>
@endsection

@stop