@extends('layouts.admin')
@section('content')
        <div class="col-md-12">
            <div class="box box-primary"><br>
                <div class="box-body">
                    <div class="form-group col-md-12">
                        @if(Session::has('success-message'))
                            <div class="alert alert-success alert-dismissible">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                <h4><i class="icon fa fa-check"></i> Alert!</h4>
                                {{ Session::get('success-message') }}
                            </div>
                        @endif
                    </div>
                    <form action="{{ route('post.register') }}" method="POST">
                        {{ csrf_field() }}

                        <div class="row">
                            <div class="col-md-6">

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                            <input type="text" name="name" class="form-control" placeholder="Full name">
                                        </div>
                                        <br>
                                    </div>

                                    <div class="col-md-6">
                                        <!-- /.box-body -->
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-user-plus"></i></span>
                                            <input type="text" name="lastname" class="form-control" placeholder="Last name">
                                        </div>
                                    </div>
                                </div>

                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                                    <input type="email" name="email" class="form-control" placeholder="Email">
                                </div>
                                <br>

                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                                    <input type="password" name="password" class="form-control" placeholder="Password">
                                </div>
                                <br>

                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-unlock"></i></span>
                                    <input type="password" name="confirm_password" class="form-control" placeholder="Confirm password">
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="Role">Role:</label>
                                            <select class="form-control" name="role">
                                                <option value="admin">admin</option>
                                                <option value="user">user</option>
                                                <option value="reviewer">reviewer</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="Status">Status:</label>
                                            <select class="form-control" name="status">
                                                <option  value="0">Passive</option>
                                                <option  value="1">Active</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="box-footer">
                                    <a href="{{ url('/admin/register') }}" style="border-radius: 3px " class="btn btn-primary btn-flat pull-left"><i class="glyphicon glyphicon-arrow-left" style="margin-right: 6px;"></i> Back </a>
                                    <button type="submit" class="btn btn-success pull-right"><i class="glyphicon glyphicon-ok" style="margin-right: 7px;"></i>Update</button>
                                </div>

                            </div>
                            <!-- /.box-body -->
                            <div class="col-md-6">
                                <img src="{{ asset('/img/register.jpg') }}" alt="" >
                            </div>
                        </div>
                    </form>
                </div>
                <!-- /.box-body -->
            </div>
        </div>
@stop

