@extends('layouts.admin')
@section('content')

    <div class="col-xs-12">

    <div class="box">
        <div class="box-header">
            <h3 class="box-title">User Management</h3>
            <a href="{{ url('admin/register/create') }}" class="btn btn-success pull-right btn-sm" ><i class="glyphicon glyphicon-plus" style="margin-right: 7px;"></i>Create</a>
            <a href="{{ url('/admin/generate-pdf') }}" class="btn btn-default  btn-sm" >PDF</a>
            <a href="{{url('product/export')}}" class="btn btn-default  btn-sm" >Excel</a>
            <a href="{{url('generate-docx')}}" class="btn btn-default  btn-sm" >Word</a>
        </div>

        <div class="form-group col-md-12">
            @if(Session::has('success-message'))
                <div class="alert alert-success alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h4><i class="icon fa fa-check"></i> Alert!</h4>
                    {{ Session::get('success-message') }}
                </div>
            @endif
        </div>


        <!-- /.box-header -->
        <div class="box-body">
            <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>Name</th>
                    <th>Lastame</th>
                    <th>Email</th>
                    <th>Status</th>
                    <th>Role</th>
                    <th>xxx</th>
                </tr>
                </thead>
                <tbody>
                @foreach($register as $item)
                <tr>
                    <td>{{ $item->name }}</td>
                    <td>{{ $item->lastname }}</td>
                    <td>{{ $item->email }}</td>

                    <td>
                        @if($item->status == '1')
                            <span class="label label-success" style="padding:5px 15px; font-size: 12px;">Active</span>
                        @elseif($item->status == '0')
                            <span class="label label-warning" style="padding:5px 12px; font-size: 12px;">Passive</span>
                        @endif
                    </td>
                    <td>{{ $item->role }}</td>
                    <td>
                        <a href="{{ url('admin/register/edit',['id'=>$item->id]) }}" class="btn btn-success btn-sm"><i class="glyphicon glyphicon-edit"> Edit</i></a>
                        <a  onclick="return confirm('Are you sure?')"  href="{{ url('admin/register/delete',['id'=>$item->id]) }}" class="btn btn-danger btn-sm"><i class="fa fa-trash"> Delete</i> </a>
                    </td>
                </tr>
                @endforeach
                </tbody>
                <tfoot>
            </table>
        </div>
        <!-- /.box-body -->
    </div>
    </div>
@stop
@section('scripts')
    <script>
        $(function () {
            $('#example1').DataTable()
            $('#example2').DataTable({
                'paging'      : true,
                'lengthChange': false,
                'searching'   : false,
                'ordering'    : true,
                'info'        : true,
                'autoWidth'   : false
            })
        })
    </script>
@stop

