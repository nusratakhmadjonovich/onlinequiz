@extends('layouts.admin')

@section('title')
    show
@stop

@section('content')
        <div class="col-xs-12">
            <div class="box">
                <!-- /.box-header -->
                <div class="box-body table-responsive no-padding">
                    <div class="col-md-6">
                        <h1 class="text-center well">To'g'ri javoblar</h1>
                        @foreach($question_from_user as $key=>$item)
                            @if($user_result->lang == 'uz')
                                {!!   $item->description_uz !!}
                            @elseif($user_result->lang == 'uz-Latn')
                                {!!   $item['description_uz-Latn'] !!}
                            @elseif($user_result->lang == 'ru')
                                {!!   $item->description_ru !!}
                            @elseif($user_result->lang == 'en')
                                {!!   $item->description_en !!}
                            @endif
                            <?php
                            if ($user_result->lang == 'uz-Latn')
                            {
                                $question =  \App\Model\Admin\Javoboz::where('savol_id','=',$item->qive_answer_id)->orderBy('id','ASC')->get();
                            }

                            if ($user_result->lang == 'uz')
                            {
                                $question =  \App\Model\Admin\Javobuz::where('savol_id','=',$item->qive_answer_id)->orderBy('id','ASC')->get();
                            }
                            if ($user_result->lang == 'ru')
                            {
                                $question =  \App\Model\Admin\Javobru::where('savol_id','=',$item->qive_answer_id)->orderBy('id','ASC')->get();
                            }

                            if ($user_result->lang == 'en')
                            {
                                $question =  \App\Model\Admin\Javoben::where('savol_id','=',$item->qive_answer_id)->orderBy('id','ASC')->get();
                            }
                            ?>
                            @foreach($question as $key2=>$item2)
                                @if($item2->togri_javob == '1')
                                    <strong style="color: green">{{ $item2->javoblar }}</strong>
                                    <hr>
                                @else
                                    <strong style="color: orange">{{ $item2->javoblar }}</strong>
                                    <hr>
                                @endif
                            @endforeach
                        @endforeach
                    </div>

                    <div class="col-md-6">
                        <h1 class="text-center well">Foydalanuvchi Javoblari</h1>
                        @foreach($question_from_user as $key=>$item)
                            @if($user_result->lang == 'uz')
                                {!!   $item['description_uz']!!}
                            @elseif($user_result->lang == 'uz-Latn')
                                {!!   $item['description_uz-Latn'] !!}
                            @elseif($user_result->lang == 'ru')
                                {!!   $item->description_ru !!}
                            @elseif($user_result->lang == 'en')
                                {!!   $item->description_en !!}
                            @endif
                            <?php
                            if ($user_result->lang == 'uz-Latn')
                            {
                                $question =  \App\Model\Admin\Javoboz::where('savol_id','=',$item->qive_answer_id)->orderBy('id','ASC')->get();
                            }

                            if ($user_result->lang == 'uz')
                            {
                                $question =  \App\Model\Admin\Javobuz::where('savol_id','=',$item->qive_answer_id)->orderBy('id','ASC')->get();
                            }

                            if ($user_result->lang == 'ru')
                            {
                                $question =  \App\Model\Admin\Javobru::where('savol_id','=',$item->qive_answer_id)->orderBy('id','ASC')->get();
                            }

                            if ($user_result->lang == 'en')
                            {
                                $question =  \App\Model\Admin\Javoben::where('savol_id','=',$item->qive_answer_id)->orderBy('id','ASC')->get();
                            }


                            ?>

                            @foreach($question as $key2=>$item2)
                                @if($item->answer_id == $item2->id)
                                    <strong style="color: green;">{{ $item2->javoblar }}</strong>
                                    <hr>
                                @else
                                    <strong style="color: orange;">{{ $item2->javoblar }}</strong>
                                    <hr>
                                @endif
                            @endforeach

                        @endforeach
                        <a href="{{ url('admin/results/users') }}" class="btn btn-default pull-right"><span class="fa fa-times-circle"></span> cencel </a>
                        <br>
                        <br>
                        <br>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <br>
        <br>
        <br>
        <br>
        <br>

@stop
@section('scripts')

@endsection