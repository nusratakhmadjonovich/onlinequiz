@extends('layouts.admin')

@section('title')
    Rezultat
@stop

@section('content')
    <div class="col-xs-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Data Table With Full Features</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <table id="example1" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th>№</th>
                        <th>Ism</th>
                        <th>Familiya</th>
                        <th>Berilgan vaqt</th>
                        <th>Test Tili</th>
                        <th>To`g`ri javob</th>
                        <th>Test Kaliti</th>
                        <th>#</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($customer as $key=>$item)
                        <?php
                        $products = \App\Model\Admin\Customer::join('users_and_results',
                            'users.id', '=', 'users_and_results.user_id')
                            ->where('users.id', '=', $item->user_id)
                            ->select('*')
                            ->first();
                        ?>
                        <tr>
                            <td>{{ $key+1 }}</td>
                            <td>{{ $products->name }}</td>
                            <td>{{ $products->lastname }}</td>
                            <td>{{ $item->created_at }}</td>
                            <td>{{ $item->lang }}</td>
                            <td>{{ $item->correct_answer_count}}</td>
                            <td>{{ $item->random_counter_id }}</td>
                            <td>
                                <a href="{{ url('admin/users/'.$item->user_id.'/answer/'.$item->random_counter_id) }}" class="btn btn-primary"><i class="glyphicon glyphicon-eye-open"></i></a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>
    <!-- /.col -->


@stop
@section('scripts')
    <script>
        $(function () {
            $('#example1').DataTable()
            $('#example2').DataTable({
                'paging'      : true,
                'lengthChange': false,
                'searching'   : false,
                'ordering'    : true,
                'info'        : true,
                'autoWidth'   : false
            })
        })
    </script>
@endsection