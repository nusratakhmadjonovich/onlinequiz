
@extends('layouts.login_register')
{{--@section('enter')
    <a href="{{ url('register') }}" class="btn btn-register"><i class="glyphicon glyphicon-user"></i>&nbsp;&nbsp;Registration</a>
@stop--}}
@section('form')
    <div class="col-sm-offset-2">
        <div class="login-box">
            <div class="login-header">
                <h2>@lang('app.home')</h2>
            </div>
            <form action="{{ route('post.login') }}" method="POST" id="login-form">
                {{ csrf_field() }}
                <div class="login-content">
                    <label class="form-label">
                        Email:
                    </label>
                    <input type="email" class="form-control" required name="email" value="{{ old('email') }}" placeholder="Email...">
                    <label class="form-label">
                        Password:
                    </label>
                    <input class="form-control" required name="password" value="" placeholder="Password..." type="password">
                    <input class="btn btn-form-login" value="Login" type="submit">
                    <a href="{{ url('register') }}" class="pull-right btn btn-form-login" style="margin-top: 20px;">Sign Up</a>
                </div>
            </form>

        </div>
    </div>
@stop