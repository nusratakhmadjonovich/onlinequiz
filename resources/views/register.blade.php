@extends('layouts.login_register')

@section('enter')
    <a href="{{ url('login') }}" class="btn btn-register"><i class="glyphicon glyphicon-user"></i>&nbsp;&nbsp;Login</a>
@stop

@section('form')
<div class="login-box">
    <div class="login-header">
        <h2>Registration</h2>
    </div>
    <form action="{{ route('post.authentication') }}" method="POST" id="login-form">
        {{ csrf_field() }}
        <div class="login-content">
            <label class="form-label">
                Firstname:
            </label>
{{--            @if ($errors->has('name'))
                <span class="text-danger">{{ $errors->first('name') }}</span>
            @endif--}}
            <div class="input-group input-blue">
                <span class="input-group-addon" id="basic-addon1"><i class="glyphicon glyphicon-user"></i></span>
                <input name="name" value="" pattern=".{2,}" required title=" " type="text" class="form-control ml-24" placeholder="Firstname..." aria-describedby="basic-addon1">
            </div>
            <label class="form-label">
                Lastname:
            </label>
            <div class="input-group input-blue">
                <span class="input-group-addon" id="basic-addon1"><i class="glyphicon glyphicon-user"></i></span>
                <input name="lastname" value="" pattern=".{2,}" required title=" " type="text" class="form-control ml-24" placeholder="Lastname..." aria-describedby="basic-addon1">
            </div>

            <label class="form-label">
                Email:
            </label>
            <div class="input-group input-blue">
                <span class="input-group-addon" id="basic-addon1"><i class="glyphicon glyphicon-envelope"></i></span>
                <input name="email" type="email" required title=" Enter the correct email" class="form-control ml-24" placeholder="Email..." aria-describedby="basic-addon1">
            </div>

            <label class="form-label">
                Password:
            </label>
            <div class="input-group input-blue">
                <span class="input-group-addon" id="basic-addon1"><i class="glyphicon glyphicon-qrcode"></i></span>
                <input name="password" id="pass" pattern=".{2,}" required title=" " type="password" class="form-control ml-24" placeholder="Password..." aria-describedby="basic-addon1">
            </div>

            <label class="form-label">
                Re-password:
            </label>
            <div class="input-group input-blue">
                <span class="input-group-addon" id="basic-addon1"><i class="glyphicon glyphicon-qrcode"></i></span>
                <input name="repassword" id="repass" onkeyup="check();" pattern=".{2,}" required title=" " type="password" class="form-control ml-24" placeholder="Password..." aria-describedby="basic-addon1">
            </div>
            <div class="alert alert-info hidden" id="passcheck">
                <p>Passwords do not match!</p>
            </div>
            <input class="btn btn-form-login" value="Sign Up" type="submit">
        </div>
    </form>

</div>
@stop