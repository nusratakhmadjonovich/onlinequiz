<?php

if (app()->getLocale() == 'uz-Latn')
    {
        $previous = \DB::table('javobozs')
            ->select('*')
            ->join('oz_users_answers', 'javobozs.id', '=', 'oz_users_answers.answer_id')
            ->where('oz_users_answers.user_id', Auth::user()->id)
            ->where('javobozs.savol_id','<',$savol->id)->max('savol_id');
    }

if (app()->getLocale() == 'uz')
    {
        $previous = \DB::table('javobuzs')
            ->select('*')
            ->join('oz_users_answers', 'javobuzs.id', '=', 'oz_users_answers.answer_id')
            ->where('oz_users_answers.user_id', Auth::user()->id)
            ->where('javobuzs.savol_id','<',$savol->id)->max('savol_id');
    }

if (app()->getLocale() == 'ru')
    {
        $previous = \DB::table('javobrus')
            ->select('*')
            ->join('oz_users_answers', 'javobrus.id', '=', 'oz_users_answers.answer_id')
            ->where('oz_users_answers.user_id', Auth::user()->id)
            ->where('javobrus.savol_id','<',$savol->id)->max('savol_id');
    }

if (app()->getLocale() == 'en')
{
    $previous = \DB::table('javobens')
        ->select('*')
        ->join('en_users_answers','javobens.id', '=', 'en_users_answers.answer_id')
        ->where('en_users_answers.user_id', Auth::user()->id)
        ->where('javobens.savol_id','<',$savol->id)->max('savol_id');
}


?>

@extends('layouts.reviewer')

@section('content')
    <div class="row">
        <div class="col-md-12 blog-main">
            <h2 class="blog-post-title"></h2>
                        <div class="alert alert-info">
                            {!! $savol->description !!}
                        </div>
            <div style="position:relative;width:100%;">
            <div id="altcontainer">
                <form action="#" id="save_privacy" method="post">
                        @foreach($javoblar as $item)
                            <label class="radiocontainer" id="label_{{$item->id}}">{{ $item->javoblar }}<input type="radio" class="answer_user" name="quiz" data-quvid="{{ $savol->id }}" value="{{ $item->id }}">
                                <span class="checkmark"></span>
                            </label>
                        @endforeach
                </form>
            </div>
            </div> <br>
            <div class="progress">
                <div class="progress-bar" role="progressbar" style="width: {{ $savol->id }}0%;" aria-valuenow="20" aria-valuemin="0" aria-valuemax="30">{{ $savol->id }}0%</div>
            </div>
            <nav>
                <ul class="pager">
                    @if($previous != 0)
                        {{--<li><a href="{{ url('reviewer/blog/'.$previous.'/'.$random) }}">&laquo; Previous</a></li>--}}
                    @endif
                        <li>
                            <form method="get" class="pull-right margin">
                               @if($next == 0)
                                    @if(app()->getLocale() =='uz')
                                        <input style="width: 140px; padding: 10px 20px; text-align: center;border-radius: 5px;background-color: #d9edf7;" type="button" data-quvid="{{ $savol->id }}" data-randomid="{{ $random }}" @if(\App\Uzuseranswer::where('user_id','=',\Illuminate\Support\Facades\Auth::user()->id)->where('qive_answer_id','=',$savol->id)->where('count_id','=',$random)->first()->answer_id == 0) id="show_result"  @endif value="Finish" />
                                    @endif
                                    @if(app()->getLocale() =='uz-Latn')
                                        <input style="width: 140px; padding: 10px 20px; text-align: center;border-radius: 5px;background-color: #d9edf7;" type="button" data-quvid="{{ $savol->id }}" data-randomid="{{ $random }}" @if(\App\Ozuseranswer::where('user_id','=',\Illuminate\Support\Facades\Auth::user()->id)->where('qive_answer_id','=',$savol->id)->where('count_id','=',$random)->first()->answer_id == 0) id="show_result"  @endif value="Finish" />
                                    @endif
                                    @if(app()->getLocale() =='ru')
                                        <input style="width: 140px; padding: 10px 20px; text-align: center;border-radius: 5px;background-color: #d9edf7;" type="button" data-quvid="{{ $savol->id }}" data-randomid="{{ $random }}" @if(\App\RUuseranswer::where('user_id','=',\Illuminate\Support\Facades\Auth::user()->id)->where('qive_answer_id','=',$savol->id)->where('count_id','=',$random)->first()->answer_id == 0) id="show_result"  @endif value="Finish" />
                                    @endif
                                    @if(app()->getLocale() =='en')
                                        <input style="width: 140px; padding: 10px 20px; text-align: center;border-radius: 5px;background-color: #d9edf7;" type="button" data-quvid="{{ $savol->id }}" data-randomid="{{ $random }}" @if(\App\ENuseranswer::where('user_id','=',\Illuminate\Support\Facades\Auth::user()->id)->where('qive_answer_id','=',$savol->id)->where('count_id','=',$random)->first()->answer_id == 0) id="show_result"  @endif value="Finish" />
                                    @endif
                                @endif
                            </form>
                        </li>
                        @if($next != 0)
                            <li><a href="#" class="pull-left" id="time">{{ $savol->time }}:00</a></li>
                            @if(app()->getLocale() == 'ru')
                                <li style="width: 240px;margin-right: 100px;"><a href="{{ url('reviewer/blog/'.$next.'/'.$random) }}" data-quvid="{{ $savol->id }}" data-randomid="{{ $random }}" @if(\App\RUuseranswer::where('user_id','=',\Illuminate\Support\Facades\Auth::user()->id)->where('qive_answer_id','=',$next)->where('count_id','=',$random)->first()->answer_id == 0) id="hisoblash" @endif class="pull-right margin next_button">Next &raquo;</a></li>
                            @endif
                            @if(app()->getLocale() == 'uz')
                                <li><a href="{{ url('reviewer/blog/'.$next.'/'.$random) }}" data-quvid="{{ $savol->id }}" data-randomid="{{ $random }}" @if(\App\Uzuseranswer::where('user_id','=',\Illuminate\Support\Facades\Auth::user()->id)->where('qive_answer_id','=',$next)->where('count_id','=',$random)->first()->answer_id == 0) id="hisoblash" @endif class="pull-right margin next_button">Next &raquo;</a></li>
                            @endif
                            @if(app()->getLocale() == 'uz-Latn')
                                <li><a href="{{ url('reviewer/blog/'.$next.'/'.$random) }}" data-quvid="{{ $savol->id }}" data-randomid="{{ $random }}" @if(\App\Ozuseranswer::where('user_id','=',\Illuminate\Support\Facades\Auth::user()->id)->where('qive_answer_id','=',$next)->where('count_id','=',$random)->first()->answer_id == 0) id="hisoblash" @endif class="pull-right margin next_button">Next &raquo;</a></li>
                            @endif
                            @if(app()->getLocale() == 'en')
                                <li><a href="{{ url('reviewer/blog/'.$next.'/'.$random) }}" data-quvid="{{ $savol->id }}" data-randomid="{{ $random }}" @if(\App\ENuseranswer::where('user_id','=',\Illuminate\Support\Facades\Auth::user()->id)->where('qive_answer_id','=',$next)->where('count_id','=',$random)->first()->answer_id == 0) id="hisoblash" @endif class="pull-right margin next_button">Next &raquo;</a></li>
                            @endif
                            @else
                            <li><a href="#" class="pull-left" id="result_time">{{ $savol->time }}:00</a></li>
                        @endif
                        <li>
                            <div id="quiz_result" style="margin-top: 49px;">

                            </div>
                        </li>

                </ul>
            </nav>
        </div><!-- /.blog-main -->
    </div><!-- /.row -->

    {{-- bootstrap collapse --}}

    <div class="collapse" id="collapseExample">
        <div class="well">
            @foreach($question_from_user as $key=>$item)
             <div class="row">
                 <h3 style="text-align: center;">{!! $item['description_uz-Latn'] !!}</h3>
             </div>
                <?php
                    if (app()->getLocale() == 'uz-Latn')
                        {
                            $question =  \App\Model\Admin\Javoboz::where('savol_id','=',$item->qive_answer_id)->orderBy('id','ASC')->get();
                        }
                    if (app()->getLocale() == 'uz')
                        {
                            $question =  \App\Model\Admin\Javobuz::where('savol_id','=',$item->qive_answer_id)->orderBy('id','ASC')->get();
                        }
                    if (app()->getLocale() == 'ru')
                        {
                            $question =  \App\Model\Admin\Javobru::where('savol_id','=',$item->qive_answer_id)->orderBy('id','ASC')->get();
                        }
                    if (app()->getLocale() == 'ru')
                        {
                            $question =  \App\Model\Admin\Javobru::where('savol_id','=',$item->qive_answer_id)->orderBy('id','ASC')->get();
                        }
                ?>
                    @foreach($question as $key2 => $item2)
                    <em style="color: orange; font-weight: bold">{{ $key2+1 }}.javob =>  </em> <strong style="color: green; font-weight: bold"> {{ $item2['javoblar'] }}</strong>
                    <hr>
                    @endforeach
            @endforeach
        </div>
    </div>

    {{-- end bootstrap collapse --}}

    {{-- Modal show--}}

    <!-- Modal -->
    <div class="modal fade" id="getCodeModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header" style="background-color: #428BCA;">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel" style="text-align: center;color: white;" > Natijalar </h4>
                </div>
                <div class="modal-body" id="getCode">
                </div>
            </div>
        </div>
    </div>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

    <script type="text/javascript">

        $(document).ready(function () {

            $('.answer_user').on('change', function() {
                var savol_id = $(this).data("quvid");
                var message_pri = $(".answer_user:checked").val();
                var auth_id = {{ \Illuminate\Support\Facades\Auth::user()->id }}
                //alert("question_id => "+savol_id+"answer_id => "+message_pri+"user_id => "+auth_id);
            });

            $('#hisoblash').on('click', function() {
                var savol_id = $(this).data("quvid");
                var random_id = $(this).data("randomid");
                var message_pri = $(".answer_user:checked").val();
                var auth_id = {{ \Illuminate\Support\Facades\Auth::user()->id }}
               // alert("question_id = > "+savol_id+"answer_id => "+message_pri+"user_id => "+auth_id);
                $.ajax({
                    type: 'GET',
                    url: "/reviewer/answer",
                    data:{message_pri,auth_id,savol_id,random_id},
                    success:function(data){
                      //  alert("success=>"+data);
                       // console.log(data.auth_id);
                    },
                    error:function (data) {
                        alert("Error");
                    }
                });
            });


            //show result

            $('#show_result').click(function () {
                var savol_id = $(this).data("quvid");
                var random_id = $(this).data("randomid");
                var message_pri = $(".answer_user:checked").val();
                var auth_id = {{ \Illuminate\Support\Facades\Auth::user()->id }}
                      //  alert('savol id=>'+savol_id+'random id=> '+random_id+'message => '+message_pri);
                $.ajax({
                    type: 'GET',
                    url: "/reviewer/result/answer",
                    data:{message_pri,auth_id,savol_id,random_id},
                    success:function(data){
                        var question = '30';
                        $("#getCode").html(
                            '<table class="table">\n' +
                            '                        <thead class="thead-light">\n' +
                            '                        <tr>\n' +
                            '                            <th scope="col">#</th>\n' +
                            '                            <th scope="col">Jami avollar</th>\n' +
                            '                            <th scope="col">To\'g\'ri javoblar</th>\n' +
                            '                            <th scope="col">Ajratilgan vaqt</th>\n' +
                            '                        </tr>\n' +
                            '                        </thead>\n' +
                            '                        <tbody>\n' +
                            '                        <tr>\n' +
                            '                            <th scope="row">1</th>\n' +
                            '                            <td>'+question+'</td>\n' +
                            '                            <td>'+data+'</td>\n' +
                            '                            <td>45:00</td>\n' +
                            '                        </tr>\n'+
                            '                        </tbody>\n' +
                            '                    </table>'
                        );
                        $("#getCodeModal").modal('show');
                        $('#quiz_result').html('<a href="{{ url('/reviewer/result/'.\Illuminate\Support\Facades\Auth::user()->id.'/'.$random) }}" style="width: 168px;border: 1px solid #337AB7;text-decoration:none; background-color: #D9EDF7;padding: 10px 5px;border-radius: 8px;float: right;text-align: center; margin-right: 10px;margin-top: -48px;">Testlar</a>');
                        $('.progress-bar').css('width','100%');
                        $('.progress-bar').text('100%');
                        $('.next_button').remove(attr('id'));
                    },
                    error:function (data) {
                        alert("Oxirgi Javobni ham belgilang ");
                    }
                });
            });

        });

        function startTimer(duration, display) {
            var timer2 = duration, minutes, seconds;
            setInterval(function () {
                minutes = parseInt(timer2 / 60, 10);
                seconds = parseInt(timer2 % 60, 10);

                minutes = minutes < 10 ? "0" + minutes : minutes;
                seconds = seconds < 10 ? "0" + seconds : seconds;

                display.textContent = minutes + ":" + seconds;

                if (minutes ==0 && seconds == 0)
                {
                    location.replace("{{ url('reviewer/blog/'.$next.'/'.$random) }}");
                }

                if (--timer2 < 0) {
                    timer2 = duration;
                }
            }, 1000);
        }

        function startTimerResult(duration, display) {
            var timer = duration, minutes, seconds;
            setInterval(function () {
                minutes = parseInt(timer / 60, 10);
                seconds = parseInt(timer % 60, 10);

                minutes = minutes < 10 ? "0" + minutes : minutes;
                seconds = seconds < 10 ? "0" + seconds : seconds;

                display.textContent = minutes + ":" + seconds;

                if (minutes ==0 && seconds == 0)
                {
                    location.replace("{{ url('/reviewer/result/'.\Illuminate\Support\Facades\Auth::user()->id.'/'.$random) }}");
                }

                if (--timer < 0) {
                    timer = duration;
                }
            }, 1000);
        }

        window.onload = function () {

            var dddd = +<?php echo $savol->time; ?>;
            var fiveMinutes = 60 * dddd;
            display = document.querySelector('#time');
            startTimer(fiveMinutes, display);

            var k = +<?php echo $savol->time; ?>;
            var fewMinutes = 60 * k;
            display = document.querySelector('#result_time');
            startTimerResult(fewMinutes, display);
        };

    </script>
@stop