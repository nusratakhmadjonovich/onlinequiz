@extends('layouts.reviewer')

@section('content')
    <div class="container">
        <div class="row">
             <div class="col-md-6">
                <h1 class="text-center well">To'g'ri javoblar</h1>
                    @foreach($question_from_user as $key=>$item)
                    {!!   $item->description !!}
                                <?php
                                 if (app()->getLocale() == 'uz-Latn')
                                     {
                                         $question =  \App\Model\Admin\Javoboz::where('savol_id','=',$item->qive_answer_id)->orderBy('id','ASC')->get();
                                     }

                                 if (app()->getLocale() == 'uz')
                                     {
                                         $question =  \App\Model\Admin\Javobuz::where('savol_id','=',$item->qive_answer_id)->orderBy('id','ASC')->get();
                                     }
                                if (app()->getLocale() == 'ru')
                                    {
                                        $question =  \App\Model\Admin\Javobru::where('savol_id','=',$item->qive_answer_id)->orderBy('id','ASC')->get();
                                    }

                                 if (app()->getLocale() == 'en')
                                 {
                                     $question =  \App\Model\Admin\Javoben::where('savol_id','=',$item->qive_answer_id)->orderBy('id','ASC')->get();
                                 }
                                ?>
                                        @foreach($question as $key2=>$item2)
                                            @if($item2->togri_javob == '1')
                                                <strong style="color: green">{{ $item2->javoblar }}</strong>
                                                <hr>
                                                @else
                                                 <strong style="color: orange">{{ $item2->javoblar }}</strong>
                                                 <hr>
                                            @endif
                                        @endforeach
                    @endforeach
             </div>

            <div class="col-md-6">
                <h1 class="text-center well">Foydalanuvchi Javoblari</h1>
                @foreach($question_from_user as $key=>$item)
                  {!! $item->description !!}
                    <?php
                    if (app()->getLocale() == 'uz-Latn')
                    {
                        $question =  \App\Model\Admin\Javoboz::where('savol_id','=',$item->qive_answer_id)->orderBy('id','ASC')->get();
                    }

                    if (app()->getLocale() == 'uz')
                        {
                            $question =  \App\Model\Admin\Javobuz::where('savol_id','=',$item->qive_answer_id)->orderBy('id','ASC')->get();
                        }

                    if (app()->getLocale() == 'ru')
                    {
                        $question =  \App\Model\Admin\Javobru::where('savol_id','=',$item->qive_answer_id)->orderBy('id','ASC')->get();
                    }

                    if (app()->getLocale() == 'en')
                    {
                        $question =  \App\Model\Admin\Javoben::where('savol_id','=',$item->qive_answer_id)->orderBy('id','ASC')->get();
                    }


                        ?>

                    @foreach($question as $key2=>$item2)
                        @if($item->answer_id == $item2->id)
                            <strong style="color: green;">{{ $item2->javoblar }}</strong>
                            <hr>
                        @else
                            <strong style="color: orange;">{{ $item2->javoblar }}</strong>
                            <hr>
                        @endif
                    @endforeach

                @endforeach
            </div>
        </div>
        <br>
        <a href="{{ url('reviewer/dashboard') }}" class="btn btn-success btn-block">yangidan test boshlash</a>
        <br>
        <br>
    </div>
@endsection