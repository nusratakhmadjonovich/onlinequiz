@extends('layouts.reviewer')

@section('content')
   <h1 class="text-center alert alert-success" style="margin-bottom: 35px;"><strong style="text-transform: uppercase;">@lang('app.reseption')</strong></h1>
    @foreach($fanlar as $science)
        <div class="alert alert-warning alert-dismissible">
            <strong>{{ $science->fanlar_name }}</strong>
            <a href="{{ route('reviewer.index',$science->id) }}" class="pull-right"><i class="glyphicon glyphicon-forward"></i></a>
        </div>
    @endforeach
@stop