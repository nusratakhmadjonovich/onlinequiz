@extends('layouts/reviewer')

@section('content')
    <div class="row">
        <div class="col-sm-12 blog-main">
            <h2 class="blog-post-title"></h2>
                @foreach($savol as $key=>$item)
                    @if($savol_id == $item->id)
                        <div class="alert alert-info">
                            {!! $item['description_uz-Latn'] !!}
                        </div>
                    @endif
                @endforeach

        <!-- <img src="paris.jpg" class="mx-auto d-block" style="width:50%">  -->
            <div style="position:relative;width:100%;">
                {{--{{ \Illuminate\Support\Facades\Auth::user() }}--}}
                <div id="altcontainer">
                    <form action="#" id="save_privacy" method="post">

                    @foreach($savol as $val)
                        @if($savol_id == $val->id)
                            @foreach( $javoblar as $item)
                                <label class="radiocontainer" id="label_{{$item->id}}">{{ $item->javoblar }}<input type="radio" class="answer_user" name="quiz" value="{{ $item->id }}">
                                    <span class="checkmark"></span>
                                </label>
                            @endforeach
                        @endif
                    @endforeach
                    </form>
                </div>
            </div> <br>
            <div class="progress">
                <div class="progress-bar" role="progressbar" style="width: 25%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">25%</div>
            </div>
            <nav>
                <ul class="pager">
                        <li><a href="{{ url('/reviewer/blog/'.$prev) }}">&laquo; Previous</a></li>
                    <li><a href="#" class="pull-right" id="time">03:00</a></li>
                    @if($next == 0)
                        <li>
                            <form method="get" style="display: inherit;" class="pull-right margin">
                                <input style="width: 140px; padding: 10px 20px; text-align: center;border-radius: 5px; padding-left: 0; background-color: #d9edf7;" type="button" id="show_result" value="show result" />
                            </form>
                        </li>
                    @endif
                        @if($next= 0)
                        <li><a href="{{ url('reviewer/blog/'.$next) }}" id="hisoblash" class="pull-right margin">Next &raquo;</a></li>
                        @endif
                </ul>
            </nav>
        </div><!-- /.blog-main -->
    </div><!-- /.row -->


    {{-- Modal show--}}

    <!-- Modal -->
    <div class="modal fade" id="getCodeModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header" style="background-color: #428BCA;">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel" style="text-align: center;color: white;" > Natijalar </h4>
                </div>
                <div class="modal-body" id="getCode">
                </div>
            </div>
        </div>
    </div>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

    <script type="text/javascript">

        $(document).ready(function () {

            $('.answer_user').on('change', function() {
                var message_pri = $(".answer_user:checked").val();
                var auth_id = {{ \Illuminate\Support\Facades\Auth::user()->id }}
                alert("answer_id => "+message_pri+"user_id => "+auth_id);
            });

            $('#hisoblash').on('click', function() {
                var message_pri = $(".answer_user:checked").val();
                var auth_id = {{ \Illuminate\Support\Facades\Auth::user()->id }}
                alert("answer_id => "+message_pri+"user_id => "+auth_id);
                $.ajax({
                    type: 'GET',
                    url: "/reviewer/answer",
                    data:{message_pri,auth_id},
                    success:function(data){
                        alert("success=>"+data);
                       // console.log(data.auth_id);
                    },
                    error:function (data) {
                        alert("Error");
                    }
                });
            });


            //show result

            $('#show_result').click(function () {
                var message_pri = $(".answer_user:checked").val();
                var auth_id = {{ \Illuminate\Support\Facades\Auth::user()->id }}

                $.ajax({
                    type: 'GET',
                    url: "/reviewer/result/answer",
                    data:{message_pri,auth_id},
                    success:function(data){
                        var question = '<?= count($savol) ?>';
                        $("#getCode").html(
                            '<table class="table">\n' +
                            '                        <thead class="thead-light">\n' +
                            '                        <tr>\n' +
                            '                            <th scope="col">#</th>\n' +
                            '                            <th scope="col">Jami avollar</th>\n' +
                            '                            <th scope="col">To\'g\'ri javoblar</th>\n' +
                            '                            <th scope="col">Ajratilgan vaqt</th>\n' +
                            '                        </tr>\n' +
                            '                        </thead>\n' +
                            '                        <tbody>\n' +
                            '                        <tr>\n' +
                            '                            <th scope="row">1</th>\n' +
                            '                            <td>'+question+'</td>\n' +
                            '                            <td>'+data+'</td>\n' +
                            '                            <td>45:00</td>\n' +
                            '                        </tr>\n'+
                            '                        </tbody>\n' +
                            '                    </table>'
                        );
                        $("#getCodeModal").modal('show');
                    },
                    error:function (data) {
                        alert("Error");
                    }
                });
            });

        });

        function startTimer(duration, display) {
            var timer = duration, minutes, seconds;
            setInterval(function () {
                minutes = parseInt(timer / 60, 10);
                seconds = parseInt(timer % 60, 10);

                minutes = minutes < 10 ? "0" + minutes : minutes;
                seconds = seconds < 10 ? "0" + seconds : seconds;

                display.textContent = minutes + ":" + seconds;

                if (--timer < 0) {
                    timer = duration;
                }
            }, 1000);
        }

        window.onload = function () {
            var fiveMinutes = 60 * 3,
                display = document.querySelector('#time');
            startTimer(fiveMinutes, display);
        };

    </script>
@stop