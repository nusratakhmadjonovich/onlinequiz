<?php if (\Illuminate\Support\Facades\Auth::user()->status != 0){ ?>

<?php

$savol = \App\Model\Admin\Articles::where('status','=',1)
    ->where('fanlar_id','=',$science->id)
    ->inRandomOrder()
    ->take($science->test_count)
    ->get();

if ($savol)
    {
        if (app()->getLocale() == 'uz-Latn')
            {
                $k = rand(1, 100000);
                foreach ($savol as $key=>$item)
                {
                    $give_answer = new \App\Ozuseranswer();
                    $give_answer->user_id = \Illuminate\Support\Facades\Auth::user()->id;
                    $give_answer->answer_id = 0;
                    $give_answer->qive_answer_id = $item->id;
                    $give_answer->count_id = $k;
                    $give_answer->lang = app()->getLocale();
                    $give_answer->save();
                }
                $next = \App\Ozuseranswer::where('user_id','=',\Illuminate\Support\Facades\Auth::user()->id)
                    ->orderBy('qive_answer_id', 'asc')
                    ->where('count_id','=',$k)
                    ->first();

            }

        if (app()->getLocale() == 'uz')
        {
            $k = rand(1, 100000);
            foreach ($savol as $key=>$item)
            {
                $give_answer = new \App\Uzuseranswer();
                $give_answer->user_id = \Illuminate\Support\Facades\Auth::user()->id;
                $give_answer->answer_id = 0;
                $give_answer->qive_answer_id = $item->id;
                $give_answer->count_id = $k;
                $give_answer->lang = app()->getLocale();
                $give_answer->save();
            }

            $next = \App\Uzuseranswer::where('user_id','=',\Illuminate\Support\Facades\Auth::user()->id)
                ->orderBy('qive_answer_id', 'asc')
                ->where('count_id','=',$k)
                ->first();
        }

        if (app()->getLocale() == 'en')
        {
            $k = rand(1, 100000);
            foreach ($savol as $key=>$item)
            {
                $give_answer = new \App\ENuseranswer();
                $give_answer->user_id = \Illuminate\Support\Facades\Auth::user()->id;
                $give_answer->answer_id = 0;
                $give_answer->qive_answer_id = $item->id;
                $give_answer->count_id = $k;
                $give_answer->lang = app()->getLocale();
                $give_answer->save();
            }

            $next = \App\ENuseranswer::where('user_id','=',\Illuminate\Support\Facades\Auth::user()->id)
                ->orderBy('qive_answer_id', 'asc')
                ->where('count_id','=',$k)
                ->first();
        }

        if (app()->getLocale() == 'ru')
        {
            $k = rand(1, 100000);
            foreach ($savol as $key=>$item)
            {
                $give_answer = new \App\RUuseranswer();
                $give_answer->user_id = \Illuminate\Support\Facades\Auth::user()->id;
                $give_answer->answer_id = 0;
                $give_answer->qive_answer_id = $item->id;
                $give_answer->count_id = $k;
                $give_answer->lang = app()->getLocale();
                $give_answer->save();
            }

            $next = \App\RUuseranswer::where('user_id','=',\Illuminate\Support\Facades\Auth::user()->id)
                ->orderBy('qive_answer_id', 'asc')
                ->where('count_id','=',$k)
                ->first();
        }
    }

?>

@extends('layouts/reviewer')
@section('content')
    <div class="jumbotron">
        <h1>@lang('app.home')</h1>
        <p>Test savollari soni: 30</p>
        <p>Har bir Savol uchun berilgan vaqt: 1 minut</p>
        <p>Tets uchun belgilangan umumiy vaqt: 30 minut</p>
        <p>
            <a class="btn btn-lg btn-primary"  id="start" href="{{ url('/reviewer/blog/'.$next->qive_answer_id.'/'.$k) }}" role="button">Testni boshlash »</a>
        </p>
    </div>
@stop

<?php }else{ ?>

<h1 class="text-center">Sizga hali admin ruxsat bermadi!</h1>

<?php } ?>