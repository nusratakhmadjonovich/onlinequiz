<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Online test</title>

    <!-- Bootstrap core CSS -->
    <link href="{{ asset('reviewer/css/bootstrap.min.css') }}" rel="stylesheet">

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="{{ asset('reviewer/css/ie10-viewport-bug-workaround.css') }}" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="{{ asset('reviewer/blog.css') }}" rel="stylesheet">
    <script src="{{ asset('reviewer/js/ie-emulation-modes-warning.js') }}"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style>
        #quizmain {
            font-family: "Segoe UI",Arial,sans-serif;
            width:100%;
            background-color:#fff;
        }
        #quizcontainer {
            padding:0 20px 40px 0;
        }
        #qtext {
            font-size:18px;
            margin-bottom:40px;
        }
        #altcontainer {
            background-color:#fff;
            font-size:120%;
            line-height:1.7em;
        }
        #answerbuttoncontainer {
            position:relative;
            padding:20px 0;
        }
        .answerbutton {
            background-color:#4CAF50;
            padding:12px 30px !important;
            font-size:17px;
        }
        #timespent {
            position:absolute;
            right:0;
            text-align:right;
            border:none;
            font-family: "Segoe UI",Arial,sans-serif;
            font-size:16px;
            width:80px;
        }
        /* The radiocontainer */
        .radiocontainer {
            background-color:#f1f1f1;
            display: block;
            position: relative;
            padding:10px 10px 10px 50px;
            margin-bottom: 1px;
            cursor: pointer;
            font-size: 18px;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
            word-wrap: break-word;
        }

        /* Hide the browser's default radio button */
        .radiocontainer input {
            position: absolute;
            opacity: 0;
            cursor: pointer;
        }

        /* Create a custom radio button */
        .checkmark {
            position: absolute;
            top: 15px;
            left: 15px;
            height: 19px;
            width: 19px;
            background-color: #fff;
            border-radius: 50%;
        }
        .checkedlabel {
            background-color:#ddd;
        }
        /* On mouse-over, add a grey background color */
        .radiocontainer:hover input ~ .checkmark {
            /*nothing*/
        }
        .radiocontainer:hover {
            background-color: #ddd;
        }

        /* When the radio button is checked, add a blue background */
        .radiocontainer input:checked ~ .checkmark {
            background-color: #2196F3;
        }

        /* Create the indicator (the dot/circle - hidden when not checked) */
        .checkmark:after {
            content: "";
            position: absolute;
            display: none;
        }

        /* Show the indicator (dot/circle) when checked */
        .radiocontainer input:checked ~ .checkmark:after {
            display: block;
        }
        /* Style the indicator (dot/circle) */
        .radiocontainer .checkmark:after {
            top: 6px;
            left: 6px;
            width: 7px;
            height: 7px;
            border-radius: 50%;
            background: white;
        }
    </style>
</head>

<body>

<div class="blog-masthead">
    <div class="container">
        <nav class="blog-nav">
            <a class="blog-nav-item active" href="#">OnlineTest</a>
            <a class="blog-nav-item active pull-right" href="{{ url('logout') }}">logout</a>
            <a href="#" class="pull-right" style="padding-right:40px;color: white;padding-top:10px;">{{ \Illuminate\Support\Facades\Auth::user()->name }}</a>
        </nav>
    </div>
</div>
</br>

<div class="container">
    @yield('content')
</div><!-- /.container -->



<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="js/jquery.min.js"><\/script>')</script>
<script src="{{ asset('reviewer/js/bootstrap.min.js') }}"></script>
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script src="{{ asset('reviewer/js/ie10-viewport-bug-workaround.js') }}"></script>
</body>
</html>
