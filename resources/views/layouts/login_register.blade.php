<!DOCTYPE html>
<html lang="sk">
<meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name='description' content=''>
    <meta name="author" content="Martina Pitáková & Dávid Duda, SOFTPAE.com">
    <title>Register</title>
    <!-- Bootstrap Core CSS -->
    <link href="{{ asset('theme/css/bootstrap.min.css') }}" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="{{ asset('theme/css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('theme/css/style2.css') }}" rel="stylesheet">
    <!-- jQuery -->
    <script src="{{ asset('theme/js/jquery.js') }}"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="{{ asset('theme/js/bootstrap.min.js') }}"></script>
    <style type="text/css">
        .language
        {
            display: inline-block;
            padding-top: 20px;
        }

        .language li
        {
            padding: 0 10px;
            float: left;
            text-decoration: none;
            list-style-type: none;
        }

        .language li a
        {
            color: white;
        }
    </style>
</head>
<body class="login register">
<div class="overlay hidden" id="overlay"></div>
<div class="container-fluid no-rt-pd">
    <div class="row">
        <div class="top-bar">
            <div class="col-lg-2 col-md-4 col-sm-12">
                <h1 class="name">OnlineTest<small class="text-white" id="man"></small></h1>
            </div>
            <div class="col-md-5 col-sm-5"></div>
            <div class="col-lg-3 col-md-5 col-sm-12 pull-right links">
                @yield('enter')
                <ul class="language">
                    @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
                        <li>
                            <a rel="alternate" hreflang="{{ $localeCode }}" href="{{ LaravelLocalization::getLocalizedURL($localeCode, null, [], true) }}">
                                {{ $properties['native'] }}
                            </a>
                        </li>
                    @endforeach
                </ul>
            </div>
        </div> <!-- END TOP BAR -->
    </div>
</div>
<div class="container">
    <div class="col-sm-12 col-md-12">
        <div class="login">
            <div class="row">
                <div class="col-sm-12">

                    @yield('form')

                </div>

            </div>
        </div>

        <script type="text/javascript">
            function check() {
                if ($("#pass").val() != $("#repass").val()) {
                    $("#passcheck").removeClass('hidden');
                } else {
                    $("#passcheck").addClass('hidden');
                }
            }
        </script>

        <!-- page end -->

    </div>
</div>

</div> <!-- END row affix-row -->
</body>
<!-- Mirrored from munka.softpae.sk/examplo/index.php?page=register by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 15 Mar 2019 10:40:49 GMT -->
</html>
