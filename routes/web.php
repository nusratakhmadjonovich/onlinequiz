<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group([
    'prefix' => LaravelLocalization::setLocale(),
    'middleware' => [ 'localeSessionRedirect', 'localizationRedirect', 'localeViewPath']
],
    function()
    {
        Route::get('/', function () {
            return view('welcome');
        });
        Route::get('register',[
            'uses'=>"User\UserController@register",
            'as'=>'ger.register'
        ]);

        Route::post('admin/register/create',[
            'uses'=>'User\UserController@post_register',
            'as'=>'post.authentication'
        ]);

        Route::get('login',[
            'uses'=>'User\UserController@login',
            'as'=>'get.login'
        ]);

        Route::post('/login' , [
            'uses'=>'User\UserController@post_login',
            'as'=>'post.login'
        ]);
        Route::get('logout',[
            'uses'=>'User\UserController@logout',
            'as'=>'get.logout'
        ]);

        Route::get('404',[
            'uses'=>'User\UserController@get_404',
            'as'=>'get.404'
        ]);

        Route::group(['namespace'=>'User','middleware'=>'user'], function(){

            Route::get('user/index',[
                'uses'=>'UserController@user_index',
                'as'=>'user.index'
            ]);

        });

        Route::group(['namespace'=>'Reviewer','middleware'=>'reviewer'], function(){

            Route::get('reviewer/index/{id}',[
                'uses'=>'ReviewerController@index',
                'as'=>'reviewer.index'
            ]);

            Route::get('reviewer/dashboard',[
                'uses'=>'ReviewerController@dashboard',
                'as'=>'reviewer.dashboard'
            ]);

            Route::get('/reviewer/blog/{id}/{random}',[
                'uses'=>'ReviewerController@blog',
                'as'=>'puzzle.question'
            ]);

            Route::get('/reviewer/answer',[
                'uses'=>'ReviewerController@user_answer',
                'as'=>'get.user.answers'
            ]);

            Route::get('/reviewer/result/answer',[
                'uses'=>'ReviewerController@user_result',
                'as'=>'get.user.result'
            ]);


            Route::post('reviewer/update/get',[
                'uses'=>'ReviewerController@get_update_answer_to'
            ]);


            Route::get('/reviewer/archive/quiz',[
                'uses'=>'ReviewerController@archive_reviewer_quiz',
                'as'=>'archive.reviewer.quiz'
            ]);

            Route::get('reviewer/result/{user_id}/{random_id}',[
                'uses'=>'TestResultsController@index',
                'as'=>'reviewer.result.index'
            ]);

        });
    });


Route::group(['namespace'=>'Admin','middleware'=>'admin'], function(){

    Route::get('admin/index', [
        'uses'=>'AdminController@admin_index',
        'as'=>'admin.index'

    ]);
    Route::get('/admin/fanlar', [
        'uses'=>'FanlarController@fanlar',
        'as'=>'get.fanlar'
    ]);
    Route::get('/admin/fanlar/creat', [
        'uses'=>'FanlarController@creat',
        'as'=>'admin.fanlar.creat'
    ]);

    Route::post('/admin/fanlar/creat', [
        'uses'=>'FanlarController@post_creat',
        'as'=>'post.creat'
    ]);
    Route::get('admin/fanlar/edit/{id}', [
        'uses'=>'FanlarController@edit',
        'as'=>'post.edit'
    ]);
    Route::post('/admin/fanlar/update/{id}',[
        'uses'=>'FanlarController@update',
        'as'=>'post.fanlar.update'
    ]);
    Route::get('/admin/fanlar/delete/{id}',[
        'uses'=>'Fanlarcontroller@delete',
        'as'=>'get.delete'
    ]);
    Route::get('/admin/results', [
        'uses'=>'AdminController@results',
        'as'=>'get.results'
    ]);
    Route::get('/admin/results2', [
        'uses'=>'AdminController@results2',
        'as'=>'get.results'
    ]);

    Route::patch('/admin/update/answer/{test}',[
        'uses'=>'ArticlesController@update_answer_to_question',
        'as'=>'post.answers.question'
    ]);

    Route::post('admin/delete/answer',[
        'uses'=>'ArticlesController@delete_answer_from_question',
        'as'=>'delete.answer.from.question'
    ]);

    Route::get('/admin/generate-pdf','AdminController@generatePDF');

    Route::get('product', 'AdminController@create');
    Route::get('product/export', 'AdminController@exportFile');

    Route::get('generate-docx', 'AdminController@generateDocx');

    Route::resource('/admin/articles','ArticlesController');

    Route::get('admin/list/articles',[
        'uses'=>'ArticlesController@list',
        'as'=>'admin.articles.list'
    ]);

    Route::get('/admin/register',[
        'uses'=>'AdminController@admin_register',
        'as'=>'admin.register'
    ]);

    Route::get('/admin/register/create',[
        'uses'=>'AdminController@admin_register_creat',
        'as'=>'admin.register.creat'
    ]);

    Route::post('admin/register/create',[
        'uses'=>'AdminController@post_register',
        'as'=>'post.register'
    ]);

    Route::get('admin/register/edit/{id}',[
        'uses'=>'AdminController@edit_register_user',
        'as'=>'admin.register.edit'
    ]);

    Route::post('admin/register/update/{id}',[
        'uses'=>'AdminController@update_register_post_user',
        'as'=>'update.post.user'
    ]);

    Route::get('admin/register/delete/{id}', [
        'uses'=>'AdminController@admin_register_delete',
        'as'=>'admin.register.delete'
    ]);

    Route::get('admin/articles/removePhoto/{id}', [
        'uses'=>'ArticlesController@removePhoto',
        'as'=>'admin.articles.removePhoto'
    ]);
    Route::get('admin/articles/removeFile/{id}', [
        'uses'=>'ArticlesController@removeFile',
        'as'=>'admin.articles.removeFile'
    ]);

    Route::get('/admin/results/users',[
        'uses'=>'ResultsController@index',
        'as'=>'admin.results.users.index'
    ]);

    Route::get('admin/users/{user_id}/answer/{random_counter_id}',[
        'uses'=>'ResultsController@admin_statistics',
        'as'=>'get.admin.statistics'
    ]);

});




